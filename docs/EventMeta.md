# EventMeta

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Addresses** | Pointer to **[]string** | A list of addresses. | [optional] 
**Index** | Pointer to **int32** | The index number. | [optional] 
**Script** | Pointer to **string** | The script. | [optional] 
**ScriptType** | Pointer to **string** | The script type. | [optional] 

## Methods

### NewEventMeta

`func NewEventMeta() *EventMeta`

NewEventMeta instantiates a new EventMeta object
This constructor will assign default values to properties that have it defined,
and makes sure properties required by API are set, but the set of arguments
will change when the set of required properties is changed

### NewEventMetaWithDefaults

`func NewEventMetaWithDefaults() *EventMeta`

NewEventMetaWithDefaults instantiates a new EventMeta object
This constructor will only assign default values to properties that have it defined,
but it doesn't guarantee that properties required by API are set

### GetAddresses

`func (o *EventMeta) GetAddresses() []string`

GetAddresses returns the Addresses field if non-nil, zero value otherwise.

### GetAddressesOk

`func (o *EventMeta) GetAddressesOk() (*[]string, bool)`

GetAddressesOk returns a tuple with the Addresses field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetAddresses

`func (o *EventMeta) SetAddresses(v []string)`

SetAddresses sets Addresses field to given value.

### HasAddresses

`func (o *EventMeta) HasAddresses() bool`

HasAddresses returns a boolean if a field has been set.

### GetIndex

`func (o *EventMeta) GetIndex() int32`

GetIndex returns the Index field if non-nil, zero value otherwise.

### GetIndexOk

`func (o *EventMeta) GetIndexOk() (*int32, bool)`

GetIndexOk returns a tuple with the Index field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetIndex

`func (o *EventMeta) SetIndex(v int32)`

SetIndex sets Index field to given value.

### HasIndex

`func (o *EventMeta) HasIndex() bool`

HasIndex returns a boolean if a field has been set.

### GetScript

`func (o *EventMeta) GetScript() string`

GetScript returns the Script field if non-nil, zero value otherwise.

### GetScriptOk

`func (o *EventMeta) GetScriptOk() (*string, bool)`

GetScriptOk returns a tuple with the Script field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetScript

`func (o *EventMeta) SetScript(v string)`

SetScript sets Script field to given value.

### HasScript

`func (o *EventMeta) HasScript() bool`

HasScript returns a boolean if a field has been set.

### GetScriptType

`func (o *EventMeta) GetScriptType() string`

GetScriptType returns the ScriptType field if non-nil, zero value otherwise.

### GetScriptTypeOk

`func (o *EventMeta) GetScriptTypeOk() (*string, bool)`

GetScriptTypeOk returns a tuple with the ScriptType field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetScriptType

`func (o *EventMeta) SetScriptType(v string)`

SetScriptType sets ScriptType field to given value.

### HasScriptType

`func (o *EventMeta) HasScriptType() bool`

HasScriptType returns a boolean if a field has been set.


[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


