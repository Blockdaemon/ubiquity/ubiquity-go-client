# FeeEstimateResponse

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Fast** | Pointer to [**EvmFee**](EvmFee.md) |  | [optional] 
**Medium** | Pointer to [**EvmFee**](EvmFee.md) |  | [optional] 
**Slow** | Pointer to [**EvmFee**](EvmFee.md) |  | [optional] 

## Methods

### NewFeeEstimateResponse

`func NewFeeEstimateResponse() *FeeEstimateResponse`

NewFeeEstimateResponse instantiates a new FeeEstimateResponse object
This constructor will assign default values to properties that have it defined,
and makes sure properties required by API are set, but the set of arguments
will change when the set of required properties is changed

### NewFeeEstimateResponseWithDefaults

`func NewFeeEstimateResponseWithDefaults() *FeeEstimateResponse`

NewFeeEstimateResponseWithDefaults instantiates a new FeeEstimateResponse object
This constructor will only assign default values to properties that have it defined,
but it doesn't guarantee that properties required by API are set

### GetFast

`func (o *FeeEstimateResponse) GetFast() EvmFee`

GetFast returns the Fast field if non-nil, zero value otherwise.

### GetFastOk

`func (o *FeeEstimateResponse) GetFastOk() (*EvmFee, bool)`

GetFastOk returns a tuple with the Fast field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetFast

`func (o *FeeEstimateResponse) SetFast(v EvmFee)`

SetFast sets Fast field to given value.

### HasFast

`func (o *FeeEstimateResponse) HasFast() bool`

HasFast returns a boolean if a field has been set.

### GetMedium

`func (o *FeeEstimateResponse) GetMedium() EvmFee`

GetMedium returns the Medium field if non-nil, zero value otherwise.

### GetMediumOk

`func (o *FeeEstimateResponse) GetMediumOk() (*EvmFee, bool)`

GetMediumOk returns a tuple with the Medium field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetMedium

`func (o *FeeEstimateResponse) SetMedium(v EvmFee)`

SetMedium sets Medium field to given value.

### HasMedium

`func (o *FeeEstimateResponse) HasMedium() bool`

HasMedium returns a boolean if a field has been set.

### GetSlow

`func (o *FeeEstimateResponse) GetSlow() EvmFee`

GetSlow returns the Slow field if non-nil, zero value otherwise.

### GetSlowOk

`func (o *FeeEstimateResponse) GetSlowOk() (*EvmFee, bool)`

GetSlowOk returns a tuple with the Slow field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetSlow

`func (o *FeeEstimateResponse) SetSlow(v EvmFee)`

SetSlow sets Slow field to given value.

### HasSlow

`func (o *FeeEstimateResponse) HasSlow() bool`

HasSlow returns a boolean if a field has been set.


[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


