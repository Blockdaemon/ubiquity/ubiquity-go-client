# TxCreateProtocol

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Avalanche** | Pointer to [**TxCreateEvm**](TxCreateEvm.md) |  | [optional] 
**Ethereum** | Pointer to [**TxCreateEvm**](TxCreateEvm.md) |  | [optional] 
**Fantom** | Pointer to [**TxCreateEvm**](TxCreateEvm.md) |  | [optional] 
**Polygon** | Pointer to [**TxCreateEvm**](TxCreateEvm.md) |  | [optional] 

## Methods

### NewTxCreateProtocol

`func NewTxCreateProtocol() *TxCreateProtocol`

NewTxCreateProtocol instantiates a new TxCreateProtocol object
This constructor will assign default values to properties that have it defined,
and makes sure properties required by API are set, but the set of arguments
will change when the set of required properties is changed

### NewTxCreateProtocolWithDefaults

`func NewTxCreateProtocolWithDefaults() *TxCreateProtocol`

NewTxCreateProtocolWithDefaults instantiates a new TxCreateProtocol object
This constructor will only assign default values to properties that have it defined,
but it doesn't guarantee that properties required by API are set

### GetAvalanche

`func (o *TxCreateProtocol) GetAvalanche() TxCreateEvm`

GetAvalanche returns the Avalanche field if non-nil, zero value otherwise.

### GetAvalancheOk

`func (o *TxCreateProtocol) GetAvalancheOk() (*TxCreateEvm, bool)`

GetAvalancheOk returns a tuple with the Avalanche field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetAvalanche

`func (o *TxCreateProtocol) SetAvalanche(v TxCreateEvm)`

SetAvalanche sets Avalanche field to given value.

### HasAvalanche

`func (o *TxCreateProtocol) HasAvalanche() bool`

HasAvalanche returns a boolean if a field has been set.

### GetEthereum

`func (o *TxCreateProtocol) GetEthereum() TxCreateEvm`

GetEthereum returns the Ethereum field if non-nil, zero value otherwise.

### GetEthereumOk

`func (o *TxCreateProtocol) GetEthereumOk() (*TxCreateEvm, bool)`

GetEthereumOk returns a tuple with the Ethereum field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetEthereum

`func (o *TxCreateProtocol) SetEthereum(v TxCreateEvm)`

SetEthereum sets Ethereum field to given value.

### HasEthereum

`func (o *TxCreateProtocol) HasEthereum() bool`

HasEthereum returns a boolean if a field has been set.

### GetFantom

`func (o *TxCreateProtocol) GetFantom() TxCreateEvm`

GetFantom returns the Fantom field if non-nil, zero value otherwise.

### GetFantomOk

`func (o *TxCreateProtocol) GetFantomOk() (*TxCreateEvm, bool)`

GetFantomOk returns a tuple with the Fantom field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetFantom

`func (o *TxCreateProtocol) SetFantom(v TxCreateEvm)`

SetFantom sets Fantom field to given value.

### HasFantom

`func (o *TxCreateProtocol) HasFantom() bool`

HasFantom returns a boolean if a field has been set.

### GetPolygon

`func (o *TxCreateProtocol) GetPolygon() TxCreateEvm`

GetPolygon returns the Polygon field if non-nil, zero value otherwise.

### GetPolygonOk

`func (o *TxCreateProtocol) GetPolygonOk() (*TxCreateEvm, bool)`

GetPolygonOk returns a tuple with the Polygon field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetPolygon

`func (o *TxCreateProtocol) SetPolygon(v TxCreateEvm)`

SetPolygon sets Polygon field to given value.

### HasPolygon

`func (o *TxCreateProtocol) HasPolygon() bool`

HasPolygon returns a boolean if a field has been set.


[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


