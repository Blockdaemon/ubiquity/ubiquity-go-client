# UnsignedTxMeta

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Fee** | Pointer to **string** | The transaction fee. | [optional] 
**SigningPayload** | Pointer to **string** | The signing payload of the transaction. | [optional] 

## Methods

### NewUnsignedTxMeta

`func NewUnsignedTxMeta() *UnsignedTxMeta`

NewUnsignedTxMeta instantiates a new UnsignedTxMeta object
This constructor will assign default values to properties that have it defined,
and makes sure properties required by API are set, but the set of arguments
will change when the set of required properties is changed

### NewUnsignedTxMetaWithDefaults

`func NewUnsignedTxMetaWithDefaults() *UnsignedTxMeta`

NewUnsignedTxMetaWithDefaults instantiates a new UnsignedTxMeta object
This constructor will only assign default values to properties that have it defined,
but it doesn't guarantee that properties required by API are set

### GetFee

`func (o *UnsignedTxMeta) GetFee() string`

GetFee returns the Fee field if non-nil, zero value otherwise.

### GetFeeOk

`func (o *UnsignedTxMeta) GetFeeOk() (*string, bool)`

GetFeeOk returns a tuple with the Fee field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetFee

`func (o *UnsignedTxMeta) SetFee(v string)`

SetFee sets Fee field to given value.

### HasFee

`func (o *UnsignedTxMeta) HasFee() bool`

HasFee returns a boolean if a field has been set.

### GetSigningPayload

`func (o *UnsignedTxMeta) GetSigningPayload() string`

GetSigningPayload returns the SigningPayload field if non-nil, zero value otherwise.

### GetSigningPayloadOk

`func (o *UnsignedTxMeta) GetSigningPayloadOk() (*string, bool)`

GetSigningPayloadOk returns a tuple with the SigningPayload field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetSigningPayload

`func (o *UnsignedTxMeta) SetSigningPayload(v string)`

SetSigningPayload sets SigningPayload field to given value.

### HasSigningPayload

`func (o *UnsignedTxMeta) HasSigningPayload() bool`

HasSigningPayload returns a boolean if a field has been set.


[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


