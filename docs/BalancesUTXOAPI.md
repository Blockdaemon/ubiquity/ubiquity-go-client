# \BalancesUTXOAPI

All URIs are relative to *https://svc.blockdaemon.com/universal/v1*

Method | HTTP request | Description
------------- | ------------- | -------------
[**GetListOfBalancesByAddress**](BalancesUTXOAPI.md#GetListOfBalancesByAddress) | **Get** /{protocol}/{network}/account/{address} | Get a List of Balances for an Address
[**GetListOfBalancesByAddresses**](BalancesUTXOAPI.md#GetListOfBalancesByAddresses) | **Post** /{protocol}/{network}/accounts/ | Get a List of Balances for Multiple Adresses
[**GetReportByAddress**](BalancesUTXOAPI.md#GetReportByAddress) | **Get** /{protocol}/{network}/account/{address}/report | Get a Financial Report for an Address Between a Time Period



## GetListOfBalancesByAddress

> []Balance GetListOfBalancesByAddress(ctx, protocol, network, address).Assets(assets).Execute()

Get a List of Balances for an Address



### Example

```go
package main

import (
    "context"
    "fmt"
    "os"
    openapiclient "./openapi"
)

func main() {
    protocol := "bitcoin" // string | Protocol handle, one of:  `algorand`, `avalanche`, `bitcoin`, `bitcoincash`, `dogecoin`, `ethereum`, `fantom`, `litecoin`, `near`, `optimism`, `polkadot`, `polygon`, `solana`, `stellar`, `tezos`, `xrp`.  (default to "bitcoin")
    network := "mainnet" // string | Which network to target. Available networks can be found in the list of supported protocols or with /{protocol}. (default to "mainnet")
    address := "1A1zP1eP5QGefi2DMPTfTL5SLmv7DivfNa" // string | The account address of the protocol. (default to "1A1zP1eP5QGefi2DMPTfTL5SLmv7DivfNa")
    assets := "ethereum/native/eth" // string | Comma-separated list of asset paths to filter. If the list is empty, or all elements are empty, this filter has no effect. Find all the asset paths on this [page](https://docs.blockdaemon.com/reference/available-currencies-and-tokens). (optional)

    configuration := openapiclient.NewConfiguration()
    api_client := openapiclient.NewAPIClient(configuration)
    resp, r, err := api_client.BalancesUTXOAPI.GetListOfBalancesByAddress(context.Background(), protocol, network, address).Assets(assets).Execute()
    if err != nil {
        fmt.Fprintf(os.Stderr, "Error when calling `BalancesUTXOAPI.GetListOfBalancesByAddress``: %v\n", err)
        fmt.Fprintf(os.Stderr, "Full HTTP response: %v\n", r)
    }
    // response from `GetListOfBalancesByAddress`: []Balance
    fmt.Fprintf(os.Stdout, "Response from `BalancesUTXOAPI.GetListOfBalancesByAddress`: %v\n", resp)
}
```

### Path Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
**ctx** | **context.Context** | context for authentication, logging, cancellation, deadlines, tracing, etc.
**protocol** | **string** | Protocol handle, one of:  &#x60;algorand&#x60;, &#x60;avalanche&#x60;, &#x60;bitcoin&#x60;, &#x60;bitcoincash&#x60;, &#x60;dogecoin&#x60;, &#x60;ethereum&#x60;, &#x60;fantom&#x60;, &#x60;litecoin&#x60;, &#x60;near&#x60;, &#x60;optimism&#x60;, &#x60;polkadot&#x60;, &#x60;polygon&#x60;, &#x60;solana&#x60;, &#x60;stellar&#x60;, &#x60;tezos&#x60;, &#x60;xrp&#x60;.  | [default to &quot;bitcoin&quot;]
**network** | **string** | Which network to target. Available networks can be found in the list of supported protocols or with /{protocol}. | [default to &quot;mainnet&quot;]
**address** | **string** | The account address of the protocol. | [default to &quot;1A1zP1eP5QGefi2DMPTfTL5SLmv7DivfNa&quot;]

### Other Parameters

Other parameters are passed through a pointer to a apiGetListOfBalancesByAddressRequest struct via the builder pattern


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------



 **assets** | **string** | Comma-separated list of asset paths to filter. If the list is empty, or all elements are empty, this filter has no effect. Find all the asset paths on this [page](https://docs.blockdaemon.com/reference/available-currencies-and-tokens). | 

### Return type

[**[]Balance**](Balance.md)

### Authorization

[apiKeyAuthHeader](../README.md#apiKeyAuthHeader), [bearerAuth](../README.md#bearerAuth)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../README.md#documentation-for-models)
[[Back to README]](../README.md)


## GetListOfBalancesByAddresses

> map[string][]Balance GetListOfBalancesByAddresses(ctx, protocol, network).AccountsObj(accountsObj).Assets(assets).Execute()

Get a List of Balances for Multiple Adresses



### Example

```go
package main

import (
    "context"
    "fmt"
    "os"
    openapiclient "./openapi"
)

func main() {
    protocol := "bitcoin" // string | Protocol handle, one of:  `algorand`, `avalanche`, `bitcoin`, `bitcoincash`, `dogecoin`, `ethereum`, `fantom`, `litecoin`, `near`, `optimism`, `polkadot`, `polygon`, `solana`, `stellar`, `tezos`, `xrp`.  (default to "bitcoin")
    network := "mainnet" // string | Which network to target. Available networks can be found in the list of supported protocols or with /{protocol}. (default to "mainnet")
    accountsObj := *openapiclient.NewAccountsObj() // AccountsObj | 
    assets := "ethereum/native/eth" // string | Comma-separated list of asset paths to filter. If the list is empty, or all elements are empty, this filter has no effect. Find all the asset paths on this [page](https://docs.blockdaemon.com/reference/available-currencies-and-tokens). (optional)

    configuration := openapiclient.NewConfiguration()
    api_client := openapiclient.NewAPIClient(configuration)
    resp, r, err := api_client.BalancesUTXOAPI.GetListOfBalancesByAddresses(context.Background(), protocol, network).AccountsObj(accountsObj).Assets(assets).Execute()
    if err != nil {
        fmt.Fprintf(os.Stderr, "Error when calling `BalancesUTXOAPI.GetListOfBalancesByAddresses``: %v\n", err)
        fmt.Fprintf(os.Stderr, "Full HTTP response: %v\n", r)
    }
    // response from `GetListOfBalancesByAddresses`: map[string][]Balance
    fmt.Fprintf(os.Stdout, "Response from `BalancesUTXOAPI.GetListOfBalancesByAddresses`: %v\n", resp)
}
```

### Path Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
**ctx** | **context.Context** | context for authentication, logging, cancellation, deadlines, tracing, etc.
**protocol** | **string** | Protocol handle, one of:  &#x60;algorand&#x60;, &#x60;avalanche&#x60;, &#x60;bitcoin&#x60;, &#x60;bitcoincash&#x60;, &#x60;dogecoin&#x60;, &#x60;ethereum&#x60;, &#x60;fantom&#x60;, &#x60;litecoin&#x60;, &#x60;near&#x60;, &#x60;optimism&#x60;, &#x60;polkadot&#x60;, &#x60;polygon&#x60;, &#x60;solana&#x60;, &#x60;stellar&#x60;, &#x60;tezos&#x60;, &#x60;xrp&#x60;.  | [default to &quot;bitcoin&quot;]
**network** | **string** | Which network to target. Available networks can be found in the list of supported protocols or with /{protocol}. | [default to &quot;mainnet&quot;]

### Other Parameters

Other parameters are passed through a pointer to a apiGetListOfBalancesByAddressesRequest struct via the builder pattern


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------


 **accountsObj** | [**AccountsObj**](AccountsObj.md) |  | 
 **assets** | **string** | Comma-separated list of asset paths to filter. If the list is empty, or all elements are empty, this filter has no effect. Find all the asset paths on this [page](https://docs.blockdaemon.com/reference/available-currencies-and-tokens). | 

### Return type

[**map[string][]Balance**](array.md)

### Authorization

[apiKeyAuthHeader](../README.md#apiKeyAuthHeader), [bearerAuth](../README.md#bearerAuth)

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../README.md#documentation-for-models)
[[Back to README]](../README.md)


## GetReportByAddress

> Report GetReportByAddress(ctx, protocol, network, address).From(from).To(to).PageToken(pageToken).PageSize(pageSize).Execute()

Get a Financial Report for an Address Between a Time Period



### Example

```go
package main

import (
    "context"
    "fmt"
    "os"
    openapiclient "./openapi"
)

func main() {
    protocol := "polkadot" // string | Protocol handle, one of: `algorand`, `fantom`, `polkadot`, `polygon`, `stellar` `tezos`, `xrp`.  (default to "polkadot")
    network := "mainnet" // string | Which network to target. Available networks can be found in the list of supported protocols or with /{protocol}. (default to "mainnet")
    address := "12yi4uHFbnSUryffXT7Xq92fbGC3iXvCs3vz9HjVgpb4sBvL" // string | The account address of the protocol. (default to "12yi4uHFbnSUryffXT7Xq92fbGC3iXvCs3vz9HjVgpb4sBvL")
    from := int32(961846434) // int32 | The Unix Timestamp from where to start. (optional)
    to := int32(1119612834) // int32 | The Unix Timestamp from where to end. (optional)
    pageToken := "xyz" // string | The token to retrieve more items in the next request. Use the `next_page_token` returned from the previous response for this parameter. (optional)
    pageSize := int32(1000) // int32 | The max number of items to return in a response. Defaults to 50k and is capped at 100k.  (optional)

    configuration := openapiclient.NewConfiguration()
    api_client := openapiclient.NewAPIClient(configuration)
    resp, r, err := api_client.BalancesUTXOAPI.GetReportByAddress(context.Background(), protocol, network, address).From(from).To(to).PageToken(pageToken).PageSize(pageSize).Execute()
    if err != nil {
        fmt.Fprintf(os.Stderr, "Error when calling `BalancesUTXOAPI.GetReportByAddress``: %v\n", err)
        fmt.Fprintf(os.Stderr, "Full HTTP response: %v\n", r)
    }
    // response from `GetReportByAddress`: Report
    fmt.Fprintf(os.Stdout, "Response from `BalancesUTXOAPI.GetReportByAddress`: %v\n", resp)
}
```

### Path Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
**ctx** | **context.Context** | context for authentication, logging, cancellation, deadlines, tracing, etc.
**protocol** | **string** | Protocol handle, one of: &#x60;algorand&#x60;, &#x60;fantom&#x60;, &#x60;polkadot&#x60;, &#x60;polygon&#x60;, &#x60;stellar&#x60; &#x60;tezos&#x60;, &#x60;xrp&#x60;.  | [default to &quot;polkadot&quot;]
**network** | **string** | Which network to target. Available networks can be found in the list of supported protocols or with /{protocol}. | [default to &quot;mainnet&quot;]
**address** | **string** | The account address of the protocol. | [default to &quot;12yi4uHFbnSUryffXT7Xq92fbGC3iXvCs3vz9HjVgpb4sBvL&quot;]

### Other Parameters

Other parameters are passed through a pointer to a apiGetReportByAddressRequest struct via the builder pattern


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------



 **from** | **int32** | The Unix Timestamp from where to start. | 
 **to** | **int32** | The Unix Timestamp from where to end. | 
 **pageToken** | **string** | The token to retrieve more items in the next request. Use the &#x60;next_page_token&#x60; returned from the previous response for this parameter. | 
 **pageSize** | **int32** | The max number of items to return in a response. Defaults to 50k and is capped at 100k.  | 

### Return type

[**Report**](Report.md)

### Authorization

[apiKeyAuthHeader](../README.md#apiKeyAuthHeader), [bearerAuth](../README.md#bearerAuth)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../README.md#documentation-for-models)
[[Back to README]](../README.md)

