# TxMinify

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Index** | Pointer to **int32** | The output index within a given transaction. | [optional] 
**TxId** | Pointer to **string** | The transaction identifier. | [optional] 
**Date** | Pointer to **int64** | The Unix timestamp. | [optional] 
**BlockId** | Pointer to **NullableString** | The ID of block. | [optional] 
**BlockNumber** | Pointer to **NullableInt64** | The height of block. | [optional] 
**Confirmations** | Pointer to **int64** | Total transaction confirmations. | [optional] 

## Methods

### NewTxMinify

`func NewTxMinify() *TxMinify`

NewTxMinify instantiates a new TxMinify object
This constructor will assign default values to properties that have it defined,
and makes sure properties required by API are set, but the set of arguments
will change when the set of required properties is changed

### NewTxMinifyWithDefaults

`func NewTxMinifyWithDefaults() *TxMinify`

NewTxMinifyWithDefaults instantiates a new TxMinify object
This constructor will only assign default values to properties that have it defined,
but it doesn't guarantee that properties required by API are set

### GetIndex

`func (o *TxMinify) GetIndex() int32`

GetIndex returns the Index field if non-nil, zero value otherwise.

### GetIndexOk

`func (o *TxMinify) GetIndexOk() (*int32, bool)`

GetIndexOk returns a tuple with the Index field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetIndex

`func (o *TxMinify) SetIndex(v int32)`

SetIndex sets Index field to given value.

### HasIndex

`func (o *TxMinify) HasIndex() bool`

HasIndex returns a boolean if a field has been set.

### GetTxId

`func (o *TxMinify) GetTxId() string`

GetTxId returns the TxId field if non-nil, zero value otherwise.

### GetTxIdOk

`func (o *TxMinify) GetTxIdOk() (*string, bool)`

GetTxIdOk returns a tuple with the TxId field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetTxId

`func (o *TxMinify) SetTxId(v string)`

SetTxId sets TxId field to given value.

### HasTxId

`func (o *TxMinify) HasTxId() bool`

HasTxId returns a boolean if a field has been set.

### GetDate

`func (o *TxMinify) GetDate() int64`

GetDate returns the Date field if non-nil, zero value otherwise.

### GetDateOk

`func (o *TxMinify) GetDateOk() (*int64, bool)`

GetDateOk returns a tuple with the Date field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetDate

`func (o *TxMinify) SetDate(v int64)`

SetDate sets Date field to given value.

### HasDate

`func (o *TxMinify) HasDate() bool`

HasDate returns a boolean if a field has been set.

### GetBlockId

`func (o *TxMinify) GetBlockId() string`

GetBlockId returns the BlockId field if non-nil, zero value otherwise.

### GetBlockIdOk

`func (o *TxMinify) GetBlockIdOk() (*string, bool)`

GetBlockIdOk returns a tuple with the BlockId field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetBlockId

`func (o *TxMinify) SetBlockId(v string)`

SetBlockId sets BlockId field to given value.

### HasBlockId

`func (o *TxMinify) HasBlockId() bool`

HasBlockId returns a boolean if a field has been set.

### SetBlockIdNil

`func (o *TxMinify) SetBlockIdNil(b bool)`

 SetBlockIdNil sets the value for BlockId to be an explicit nil

### UnsetBlockId
`func (o *TxMinify) UnsetBlockId()`

UnsetBlockId ensures that no value is present for BlockId, not even an explicit nil
### GetBlockNumber

`func (o *TxMinify) GetBlockNumber() int64`

GetBlockNumber returns the BlockNumber field if non-nil, zero value otherwise.

### GetBlockNumberOk

`func (o *TxMinify) GetBlockNumberOk() (*int64, bool)`

GetBlockNumberOk returns a tuple with the BlockNumber field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetBlockNumber

`func (o *TxMinify) SetBlockNumber(v int64)`

SetBlockNumber sets BlockNumber field to given value.

### HasBlockNumber

`func (o *TxMinify) HasBlockNumber() bool`

HasBlockNumber returns a boolean if a field has been set.

### SetBlockNumberNil

`func (o *TxMinify) SetBlockNumberNil(b bool)`

 SetBlockNumberNil sets the value for BlockNumber to be an explicit nil

### UnsetBlockNumber
`func (o *TxMinify) UnsetBlockNumber()`

UnsetBlockNumber ensures that no value is present for BlockNumber, not even an explicit nil
### GetConfirmations

`func (o *TxMinify) GetConfirmations() int64`

GetConfirmations returns the Confirmations field if non-nil, zero value otherwise.

### GetConfirmationsOk

`func (o *TxMinify) GetConfirmationsOk() (*int64, bool)`

GetConfirmationsOk returns a tuple with the Confirmations field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetConfirmations

`func (o *TxMinify) SetConfirmations(v int64)`

SetConfirmations sets Confirmations field to given value.

### HasConfirmations

`func (o *TxMinify) HasConfirmations() bool`

HasConfirmations returns a boolean if a field has been set.


[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


