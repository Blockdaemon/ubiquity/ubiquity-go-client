# InlineResponse200

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**MostRecentBlock** | Pointer to **int32** |  | [optional] 
**EstimatedFees** | Pointer to [**FeeEstimateResponse**](FeeEstimateResponse.md) |  | [optional] 

## Methods

### NewInlineResponse200

`func NewInlineResponse200() *InlineResponse200`

NewInlineResponse200 instantiates a new InlineResponse200 object
This constructor will assign default values to properties that have it defined,
and makes sure properties required by API are set, but the set of arguments
will change when the set of required properties is changed

### NewInlineResponse200WithDefaults

`func NewInlineResponse200WithDefaults() *InlineResponse200`

NewInlineResponse200WithDefaults instantiates a new InlineResponse200 object
This constructor will only assign default values to properties that have it defined,
but it doesn't guarantee that properties required by API are set

### GetMostRecentBlock

`func (o *InlineResponse200) GetMostRecentBlock() int32`

GetMostRecentBlock returns the MostRecentBlock field if non-nil, zero value otherwise.

### GetMostRecentBlockOk

`func (o *InlineResponse200) GetMostRecentBlockOk() (*int32, bool)`

GetMostRecentBlockOk returns a tuple with the MostRecentBlock field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetMostRecentBlock

`func (o *InlineResponse200) SetMostRecentBlock(v int32)`

SetMostRecentBlock sets MostRecentBlock field to given value.

### HasMostRecentBlock

`func (o *InlineResponse200) HasMostRecentBlock() bool`

HasMostRecentBlock returns a boolean if a field has been set.

### GetEstimatedFees

`func (o *InlineResponse200) GetEstimatedFees() FeeEstimateResponse`

GetEstimatedFees returns the EstimatedFees field if non-nil, zero value otherwise.

### GetEstimatedFeesOk

`func (o *InlineResponse200) GetEstimatedFeesOk() (*FeeEstimateResponse, bool)`

GetEstimatedFeesOk returns a tuple with the EstimatedFees field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetEstimatedFees

`func (o *InlineResponse200) SetEstimatedFees(v FeeEstimateResponse)`

SetEstimatedFees sets EstimatedFees field to given value.

### HasEstimatedFees

`func (o *InlineResponse200) HasEstimatedFees() bool`

HasEstimatedFees returns a boolean if a field has been set.


[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


