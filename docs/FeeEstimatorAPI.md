# \FeeEstimatorAPI

All URIs are relative to *https://svc.blockdaemon.com/universal/v1*

Method | HTTP request | Description
------------- | ------------- | -------------
[**GetFeeEstimate**](FeeEstimatorAPI.md#GetFeeEstimate) | **Get** /{protocol}/{network}/tx/estimate_fee | Get the Fee Estimation



## GetFeeEstimate

> InlineResponse200 GetFeeEstimate(ctx, protocol, network).Execute()

Get the Fee Estimation



### Example

```go
package main

import (
    "context"
    "fmt"
    "os"
    openapiclient "./openapi"
)

func main() {
    protocol := "bitcoin" // string | Protocol handle, one of: `avalanche`, `bitcoin`, `bitcoincash`, `ethereum`, `fantom`, `litecoin`, `polkadot`, `polygon` and `solana`.  (default to "bitcoin")
    network := "mainnet" // string | Which network to target. Available networks can be found in the list of supported protocols or with /{protocol}. (default to "mainnet")

    configuration := openapiclient.NewConfiguration()
    api_client := openapiclient.NewAPIClient(configuration)
    resp, r, err := api_client.FeeEstimatorAPI.GetFeeEstimate(context.Background(), protocol, network).Execute()
    if err != nil {
        fmt.Fprintf(os.Stderr, "Error when calling `FeeEstimatorAPI.GetFeeEstimate``: %v\n", err)
        fmt.Fprintf(os.Stderr, "Full HTTP response: %v\n", r)
    }
    // response from `GetFeeEstimate`: InlineResponse200
    fmt.Fprintf(os.Stdout, "Response from `FeeEstimatorAPI.GetFeeEstimate`: %v\n", resp)
}
```

### Path Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
**ctx** | **context.Context** | context for authentication, logging, cancellation, deadlines, tracing, etc.
**protocol** | **string** | Protocol handle, one of: &#x60;avalanche&#x60;, &#x60;bitcoin&#x60;, &#x60;bitcoincash&#x60;, &#x60;ethereum&#x60;, &#x60;fantom&#x60;, &#x60;litecoin&#x60;, &#x60;polkadot&#x60;, &#x60;polygon&#x60; and &#x60;solana&#x60;.  | [default to &quot;bitcoin&quot;]
**network** | **string** | Which network to target. Available networks can be found in the list of supported protocols or with /{protocol}. | [default to &quot;mainnet&quot;]

### Other Parameters

Other parameters are passed through a pointer to a apiGetFeeEstimateRequest struct via the builder pattern


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------



### Return type

[**InlineResponse200**](InlineResponse200.md)

### Authorization

[apiKeyAuthHeader](../README.md#apiKeyAuthHeader), [bearerAuth](../README.md#bearerAuth)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../README.md#documentation-for-models)
[[Back to README]](../README.md)

