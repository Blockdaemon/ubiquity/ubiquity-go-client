# FeeEstimate

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Fast** | Pointer to **bigInt** | The fast maximum priority fee. | [optional] 
**Medium** | Pointer to **bigInt** | The medium maximum priority fee. | [optional] 
**Slow** | Pointer to **bigInt** | The slow maximum priority fee. | [optional] 

## Methods

### NewFeeEstimate

`func NewFeeEstimate() *FeeEstimate`

NewFeeEstimate instantiates a new FeeEstimate object
This constructor will assign default values to properties that have it defined,
and makes sure properties required by API are set, but the set of arguments
will change when the set of required properties is changed

### NewFeeEstimateWithDefaults

`func NewFeeEstimateWithDefaults() *FeeEstimate`

NewFeeEstimateWithDefaults instantiates a new FeeEstimate object
This constructor will only assign default values to properties that have it defined,
but it doesn't guarantee that properties required by API are set

### GetFast

`func (o *FeeEstimate) GetFast() bigInt`

GetFast returns the Fast field if non-nil, zero value otherwise.

### GetFastOk

`func (o *FeeEstimate) GetFastOk() (*bigInt, bool)`

GetFastOk returns a tuple with the Fast field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetFast

`func (o *FeeEstimate) SetFast(v bigInt)`

SetFast sets Fast field to given value.

### HasFast

`func (o *FeeEstimate) HasFast() bool`

HasFast returns a boolean if a field has been set.

### GetMedium

`func (o *FeeEstimate) GetMedium() bigInt`

GetMedium returns the Medium field if non-nil, zero value otherwise.

### GetMediumOk

`func (o *FeeEstimate) GetMediumOk() (*bigInt, bool)`

GetMediumOk returns a tuple with the Medium field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetMedium

`func (o *FeeEstimate) SetMedium(v bigInt)`

SetMedium sets Medium field to given value.

### HasMedium

`func (o *FeeEstimate) HasMedium() bool`

HasMedium returns a boolean if a field has been set.

### GetSlow

`func (o *FeeEstimate) GetSlow() bigInt`

GetSlow returns the Slow field if non-nil, zero value otherwise.

### GetSlowOk

`func (o *FeeEstimate) GetSlowOk() (*bigInt, bool)`

GetSlowOk returns a tuple with the Slow field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetSlow

`func (o *FeeEstimate) SetSlow(v bigInt)`

SetSlow sets Slow field to given value.

### HasSlow

`func (o *FeeEstimate) HasSlow() bool`

HasSlow returns a boolean if a field has been set.


[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


