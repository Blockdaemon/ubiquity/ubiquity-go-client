# TxOutputResponseMeta

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Addresses** | Pointer to **[]string** | A list of addresses. | [optional] 
**Index** | Pointer to **int32** | The index number. | [optional] 
**Script** | Pointer to **string** | The script. | [optional] 
**ScriptType** | Pointer to **string** | The script type. | [optional] 

## Methods

### NewTxOutputResponseMeta

`func NewTxOutputResponseMeta() *TxOutputResponseMeta`

NewTxOutputResponseMeta instantiates a new TxOutputResponseMeta object
This constructor will assign default values to properties that have it defined,
and makes sure properties required by API are set, but the set of arguments
will change when the set of required properties is changed

### NewTxOutputResponseMetaWithDefaults

`func NewTxOutputResponseMetaWithDefaults() *TxOutputResponseMeta`

NewTxOutputResponseMetaWithDefaults instantiates a new TxOutputResponseMeta object
This constructor will only assign default values to properties that have it defined,
but it doesn't guarantee that properties required by API are set

### GetAddresses

`func (o *TxOutputResponseMeta) GetAddresses() []string`

GetAddresses returns the Addresses field if non-nil, zero value otherwise.

### GetAddressesOk

`func (o *TxOutputResponseMeta) GetAddressesOk() (*[]string, bool)`

GetAddressesOk returns a tuple with the Addresses field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetAddresses

`func (o *TxOutputResponseMeta) SetAddresses(v []string)`

SetAddresses sets Addresses field to given value.

### HasAddresses

`func (o *TxOutputResponseMeta) HasAddresses() bool`

HasAddresses returns a boolean if a field has been set.

### GetIndex

`func (o *TxOutputResponseMeta) GetIndex() int32`

GetIndex returns the Index field if non-nil, zero value otherwise.

### GetIndexOk

`func (o *TxOutputResponseMeta) GetIndexOk() (*int32, bool)`

GetIndexOk returns a tuple with the Index field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetIndex

`func (o *TxOutputResponseMeta) SetIndex(v int32)`

SetIndex sets Index field to given value.

### HasIndex

`func (o *TxOutputResponseMeta) HasIndex() bool`

HasIndex returns a boolean if a field has been set.

### GetScript

`func (o *TxOutputResponseMeta) GetScript() string`

GetScript returns the Script field if non-nil, zero value otherwise.

### GetScriptOk

`func (o *TxOutputResponseMeta) GetScriptOk() (*string, bool)`

GetScriptOk returns a tuple with the Script field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetScript

`func (o *TxOutputResponseMeta) SetScript(v string)`

SetScript sets Script field to given value.

### HasScript

`func (o *TxOutputResponseMeta) HasScript() bool`

HasScript returns a boolean if a field has been set.

### GetScriptType

`func (o *TxOutputResponseMeta) GetScriptType() string`

GetScriptType returns the ScriptType field if non-nil, zero value otherwise.

### GetScriptTypeOk

`func (o *TxOutputResponseMeta) GetScriptTypeOk() (*string, bool)`

GetScriptTypeOk returns a tuple with the ScriptType field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetScriptType

`func (o *TxOutputResponseMeta) SetScriptType(v string)`

SetScriptType sets ScriptType field to given value.

### HasScriptType

`func (o *TxOutputResponseMeta) HasScriptType() bool`

HasScriptType returns a boolean if a field has been set.


[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


