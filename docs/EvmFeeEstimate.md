# EvmFeeEstimate

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Fast** | Pointer to [**EvmFee**](EvmFee.md) |  | [optional] 
**Medium** | Pointer to [**EvmFee**](EvmFee.md) |  | [optional] 
**Slow** | Pointer to [**EvmFee**](EvmFee.md) |  | [optional] 

## Methods

### NewEvmFeeEstimate

`func NewEvmFeeEstimate() *EvmFeeEstimate`

NewEvmFeeEstimate instantiates a new EvmFeeEstimate object
This constructor will assign default values to properties that have it defined,
and makes sure properties required by API are set, but the set of arguments
will change when the set of required properties is changed

### NewEvmFeeEstimateWithDefaults

`func NewEvmFeeEstimateWithDefaults() *EvmFeeEstimate`

NewEvmFeeEstimateWithDefaults instantiates a new EvmFeeEstimate object
This constructor will only assign default values to properties that have it defined,
but it doesn't guarantee that properties required by API are set

### GetFast

`func (o *EvmFeeEstimate) GetFast() EvmFee`

GetFast returns the Fast field if non-nil, zero value otherwise.

### GetFastOk

`func (o *EvmFeeEstimate) GetFastOk() (*EvmFee, bool)`

GetFastOk returns a tuple with the Fast field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetFast

`func (o *EvmFeeEstimate) SetFast(v EvmFee)`

SetFast sets Fast field to given value.

### HasFast

`func (o *EvmFeeEstimate) HasFast() bool`

HasFast returns a boolean if a field has been set.

### GetMedium

`func (o *EvmFeeEstimate) GetMedium() EvmFee`

GetMedium returns the Medium field if non-nil, zero value otherwise.

### GetMediumOk

`func (o *EvmFeeEstimate) GetMediumOk() (*EvmFee, bool)`

GetMediumOk returns a tuple with the Medium field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetMedium

`func (o *EvmFeeEstimate) SetMedium(v EvmFee)`

SetMedium sets Medium field to given value.

### HasMedium

`func (o *EvmFeeEstimate) HasMedium() bool`

HasMedium returns a boolean if a field has been set.

### GetSlow

`func (o *EvmFeeEstimate) GetSlow() EvmFee`

GetSlow returns the Slow field if non-nil, zero value otherwise.

### GetSlowOk

`func (o *EvmFeeEstimate) GetSlowOk() (*EvmFee, bool)`

GetSlowOk returns a tuple with the Slow field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetSlow

`func (o *EvmFeeEstimate) SetSlow(v EvmFee)`

SetSlow sets Slow field to given value.

### HasSlow

`func (o *EvmFeeEstimate) HasSlow() bool`

HasSlow returns a boolean if a field has been set.


[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


