# EvmFee

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**MaxPriorityFee** | Pointer to **bigInt** | The fast slow priority fee. | [optional] 
**MaxTotalFee** | Pointer to **bigInt** | The fast slow total fee. | [optional] 

## Methods

### NewEvmFee

`func NewEvmFee() *EvmFee`

NewEvmFee instantiates a new EvmFee object
This constructor will assign default values to properties that have it defined,
and makes sure properties required by API are set, but the set of arguments
will change when the set of required properties is changed

### NewEvmFeeWithDefaults

`func NewEvmFeeWithDefaults() *EvmFee`

NewEvmFeeWithDefaults instantiates a new EvmFee object
This constructor will only assign default values to properties that have it defined,
but it doesn't guarantee that properties required by API are set

### GetMaxPriorityFee

`func (o *EvmFee) GetMaxPriorityFee() bigInt`

GetMaxPriorityFee returns the MaxPriorityFee field if non-nil, zero value otherwise.

### GetMaxPriorityFeeOk

`func (o *EvmFee) GetMaxPriorityFeeOk() (*bigInt, bool)`

GetMaxPriorityFeeOk returns a tuple with the MaxPriorityFee field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetMaxPriorityFee

`func (o *EvmFee) SetMaxPriorityFee(v bigInt)`

SetMaxPriorityFee sets MaxPriorityFee field to given value.

### HasMaxPriorityFee

`func (o *EvmFee) HasMaxPriorityFee() bool`

HasMaxPriorityFee returns a boolean if a field has been set.

### GetMaxTotalFee

`func (o *EvmFee) GetMaxTotalFee() bigInt`

GetMaxTotalFee returns the MaxTotalFee field if non-nil, zero value otherwise.

### GetMaxTotalFeeOk

`func (o *EvmFee) GetMaxTotalFeeOk() (*bigInt, bool)`

GetMaxTotalFeeOk returns a tuple with the MaxTotalFee field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetMaxTotalFee

`func (o *EvmFee) SetMaxTotalFee(v bigInt)`

SetMaxTotalFee sets MaxTotalFee field to given value.

### HasMaxTotalFee

`func (o *EvmFee) HasMaxTotalFee() bool`

HasMaxTotalFee returns a boolean if a field has been set.


[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


