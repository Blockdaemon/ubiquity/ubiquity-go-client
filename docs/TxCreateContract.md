# TxCreateContract

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Address** | Pointer to **string** | The address of the contract. | [optional] 
**Type** | Pointer to **string** | The type of contract. | [optional] 

## Methods

### NewTxCreateContract

`func NewTxCreateContract() *TxCreateContract`

NewTxCreateContract instantiates a new TxCreateContract object
This constructor will assign default values to properties that have it defined,
and makes sure properties required by API are set, but the set of arguments
will change when the set of required properties is changed

### NewTxCreateContractWithDefaults

`func NewTxCreateContractWithDefaults() *TxCreateContract`

NewTxCreateContractWithDefaults instantiates a new TxCreateContract object
This constructor will only assign default values to properties that have it defined,
but it doesn't guarantee that properties required by API are set

### GetAddress

`func (o *TxCreateContract) GetAddress() string`

GetAddress returns the Address field if non-nil, zero value otherwise.

### GetAddressOk

`func (o *TxCreateContract) GetAddressOk() (*string, bool)`

GetAddressOk returns a tuple with the Address field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetAddress

`func (o *TxCreateContract) SetAddress(v string)`

SetAddress sets Address field to given value.

### HasAddress

`func (o *TxCreateContract) HasAddress() bool`

HasAddress returns a boolean if a field has been set.

### GetType

`func (o *TxCreateContract) GetType() string`

GetType returns the Type field if non-nil, zero value otherwise.

### GetTypeOk

`func (o *TxCreateContract) GetTypeOk() (*string, bool)`

GetTypeOk returns a tuple with the Type field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetType

`func (o *TxCreateContract) SetType(v string)`

SetType sets Type field to given value.

### HasType

`func (o *TxCreateContract) HasType() bool`

HasType returns a boolean if a field has been set.


[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


