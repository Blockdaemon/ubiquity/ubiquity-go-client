# ProtocolsOverview

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Protocols** | Pointer to [**[]ProtocolsOverviewProtocols**](ProtocolsOverviewProtocols.md) | List of items each describing a pair of supported protocol and network. | [optional] 

## Methods

### NewProtocolsOverview

`func NewProtocolsOverview() *ProtocolsOverview`

NewProtocolsOverview instantiates a new ProtocolsOverview object
This constructor will assign default values to properties that have it defined,
and makes sure properties required by API are set, but the set of arguments
will change when the set of required properties is changed

### NewProtocolsOverviewWithDefaults

`func NewProtocolsOverviewWithDefaults() *ProtocolsOverview`

NewProtocolsOverviewWithDefaults instantiates a new ProtocolsOverview object
This constructor will only assign default values to properties that have it defined,
but it doesn't guarantee that properties required by API are set

### GetProtocols

`func (o *ProtocolsOverview) GetProtocols() []ProtocolsOverviewProtocols`

GetProtocols returns the Protocols field if non-nil, zero value otherwise.

### GetProtocolsOk

`func (o *ProtocolsOverview) GetProtocolsOk() (*[]ProtocolsOverviewProtocols, bool)`

GetProtocolsOk returns a tuple with the Protocols field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetProtocols

`func (o *ProtocolsOverview) SetProtocols(v []ProtocolsOverviewProtocols)`

SetProtocols sets Protocols field to given value.

### HasProtocols

`func (o *ProtocolsOverview) HasProtocols() bool`

HasProtocols returns a boolean if a field has been set.


[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


