# ProtocolDetail

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Handle** | Pointer to **string** | The protocol handle. | [optional] 
**Network** | Pointer to **interface{}** | The specified network type. | [optional] 
**GenesisNumber** | Pointer to **int64** | The genesis number. | [optional] 
**Endpoints** | Pointer to **[]string** | The supported endpoints. | [optional] 

## Methods

### NewProtocolDetail

`func NewProtocolDetail() *ProtocolDetail`

NewProtocolDetail instantiates a new ProtocolDetail object
This constructor will assign default values to properties that have it defined,
and makes sure properties required by API are set, but the set of arguments
will change when the set of required properties is changed

### NewProtocolDetailWithDefaults

`func NewProtocolDetailWithDefaults() *ProtocolDetail`

NewProtocolDetailWithDefaults instantiates a new ProtocolDetail object
This constructor will only assign default values to properties that have it defined,
but it doesn't guarantee that properties required by API are set

### GetHandle

`func (o *ProtocolDetail) GetHandle() string`

GetHandle returns the Handle field if non-nil, zero value otherwise.

### GetHandleOk

`func (o *ProtocolDetail) GetHandleOk() (*string, bool)`

GetHandleOk returns a tuple with the Handle field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetHandle

`func (o *ProtocolDetail) SetHandle(v string)`

SetHandle sets Handle field to given value.

### HasHandle

`func (o *ProtocolDetail) HasHandle() bool`

HasHandle returns a boolean if a field has been set.

### GetNetwork

`func (o *ProtocolDetail) GetNetwork() interface{}`

GetNetwork returns the Network field if non-nil, zero value otherwise.

### GetNetworkOk

`func (o *ProtocolDetail) GetNetworkOk() (*interface{}, bool)`

GetNetworkOk returns a tuple with the Network field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetNetwork

`func (o *ProtocolDetail) SetNetwork(v interface{})`

SetNetwork sets Network field to given value.

### HasNetwork

`func (o *ProtocolDetail) HasNetwork() bool`

HasNetwork returns a boolean if a field has been set.

### SetNetworkNil

`func (o *ProtocolDetail) SetNetworkNil(b bool)`

 SetNetworkNil sets the value for Network to be an explicit nil

### UnsetNetwork
`func (o *ProtocolDetail) UnsetNetwork()`

UnsetNetwork ensures that no value is present for Network, not even an explicit nil
### GetGenesisNumber

`func (o *ProtocolDetail) GetGenesisNumber() int64`

GetGenesisNumber returns the GenesisNumber field if non-nil, zero value otherwise.

### GetGenesisNumberOk

`func (o *ProtocolDetail) GetGenesisNumberOk() (*int64, bool)`

GetGenesisNumberOk returns a tuple with the GenesisNumber field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetGenesisNumber

`func (o *ProtocolDetail) SetGenesisNumber(v int64)`

SetGenesisNumber sets GenesisNumber field to given value.

### HasGenesisNumber

`func (o *ProtocolDetail) HasGenesisNumber() bool`

HasGenesisNumber returns a boolean if a field has been set.

### GetEndpoints

`func (o *ProtocolDetail) GetEndpoints() []string`

GetEndpoints returns the Endpoints field if non-nil, zero value otherwise.

### GetEndpointsOk

`func (o *ProtocolDetail) GetEndpointsOk() (*[]string, bool)`

GetEndpointsOk returns a tuple with the Endpoints field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetEndpoints

`func (o *ProtocolDetail) SetEndpoints(v []string)`

SetEndpoints sets Endpoints field to given value.

### HasEndpoints

`func (o *ProtocolDetail) HasEndpoints() bool`

HasEndpoints returns a boolean if a field has been set.


[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


