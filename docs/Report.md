# Report

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Fields** | [**[]ReportField**](ReportField.md) | The transaction items. | 
**Items** | **int32** | The number of transactions in the report. | 
**Limit** | Pointer to **int32** | The limit number. | [optional] 
**PageSize** | Pointer to **int32** | The limit number provided in the request or the default. | [optional] 
**PageToken** | Pointer to **string** | The token to retrieve more items in the next request. Use the &#x60;next_page_token&#x60; returned from the previous response for this parameter. | [optional] 

## Methods

### NewReport

`func NewReport(fields []ReportField, items int32, ) *Report`

NewReport instantiates a new Report object
This constructor will assign default values to properties that have it defined,
and makes sure properties required by API are set, but the set of arguments
will change when the set of required properties is changed

### NewReportWithDefaults

`func NewReportWithDefaults() *Report`

NewReportWithDefaults instantiates a new Report object
This constructor will only assign default values to properties that have it defined,
but it doesn't guarantee that properties required by API are set

### GetFields

`func (o *Report) GetFields() []ReportField`

GetFields returns the Fields field if non-nil, zero value otherwise.

### GetFieldsOk

`func (o *Report) GetFieldsOk() (*[]ReportField, bool)`

GetFieldsOk returns a tuple with the Fields field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetFields

`func (o *Report) SetFields(v []ReportField)`

SetFields sets Fields field to given value.


### GetItems

`func (o *Report) GetItems() int32`

GetItems returns the Items field if non-nil, zero value otherwise.

### GetItemsOk

`func (o *Report) GetItemsOk() (*int32, bool)`

GetItemsOk returns a tuple with the Items field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetItems

`func (o *Report) SetItems(v int32)`

SetItems sets Items field to given value.


### GetLimit

`func (o *Report) GetLimit() int32`

GetLimit returns the Limit field if non-nil, zero value otherwise.

### GetLimitOk

`func (o *Report) GetLimitOk() (*int32, bool)`

GetLimitOk returns a tuple with the Limit field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetLimit

`func (o *Report) SetLimit(v int32)`

SetLimit sets Limit field to given value.

### HasLimit

`func (o *Report) HasLimit() bool`

HasLimit returns a boolean if a field has been set.

### GetPageSize

`func (o *Report) GetPageSize() int32`

GetPageSize returns the PageSize field if non-nil, zero value otherwise.

### GetPageSizeOk

`func (o *Report) GetPageSizeOk() (*int32, bool)`

GetPageSizeOk returns a tuple with the PageSize field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetPageSize

`func (o *Report) SetPageSize(v int32)`

SetPageSize sets PageSize field to given value.

### HasPageSize

`func (o *Report) HasPageSize() bool`

HasPageSize returns a boolean if a field has been set.

### GetPageToken

`func (o *Report) GetPageToken() string`

GetPageToken returns the PageToken field if non-nil, zero value otherwise.

### GetPageTokenOk

`func (o *Report) GetPageTokenOk() (*string, bool)`

GetPageTokenOk returns a tuple with the PageToken field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetPageToken

`func (o *Report) SetPageToken(v string)`

SetPageToken sets PageToken field to given value.

### HasPageToken

`func (o *Report) HasPageToken() bool`

HasPageToken returns a boolean if a field has been set.


[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


