module gitlab.com/Blockdaemon/ubiquity/ubiquity-go-client/examples/current-block

go 1.22

toolchain go1.22.7

require gitlab.com/Blockdaemon/ubiquity/ubiquity-go-client v0.0.0

require golang.org/x/oauth2 v0.24.0 // indirect

replace gitlab.com/Blockdaemon/ubiquity/ubiquity-go-client v0.0.0 => ../../
