package main

import (
	"context"
	"fmt"
	"os"

	"gitlab.com/Blockdaemon/ubiquity/ubiquity-go-client/examples"
	ubiquity "gitlab.com/Blockdaemon/ubiquity/ubiquity-go-client/v1/pkg/client"
)

/*
*
Getting current block and iterating over transactions.

Env variables:
 1. UBI_ACCESS_TOKEN - required, Ubiquity API Access Token
 2. UBI_ENDPOINT - optional, Ubiquity API custom endpoint (prod by default)
 3. UBI_PROTOCOL - optional, protocol e.g. ethereum (bitcoin by default)
*/
func main() {
	// Access token is required
	accessToken := os.Getenv("UBI_ACCESS_TOKEN")
	if accessToken == "" {
		panic(fmt.Errorf("env variable 'UBI_ACCESS_TOKEN' must be set"))
	}

	config := ubiquity.NewConfiguration()

	// You can *optionally* set a custom server/endpoint,
	// or it will use the default that it's prod `https://svc.blockdaemon.com/universal/v1`
	if endpoint := os.Getenv("UBI_ENDPOINT"); endpoint != "" {
		config.Servers = ubiquity.ServerConfigurations{
			{
				URL:         endpoint,
				Description: "Custom endpoint",
			},
		}
	}

	var protocol string
	if protocol = os.Getenv("UBI_PROTOCOL"); protocol == "" {
		protocol = "bitcoin"
	}

	apiClient := ubiquity.NewAPIClient(config)

	// Setting the access token or api key to the context
	ctx := context.WithValue(context.Background(), ubiquity.ContextAccessToken, accessToken)

	// Getting a current block
	block, resp, err := apiClient.BlocksAPI.GetBlockByNumber(ctx, protocol, "mainnet", "current").Execute()
	if err != nil {
		panic(fmt.Errorf("failed to get a current %s block: got status '%s' and error '%v'",
			protocol, resp.Status, err))
	}

	fmt.Printf("Current %s block comes with ID '%s' and transactions count %d\n",
		protocol, block.GetId(), len(block.GetTxs()))

	if len(block.GetTxs()) > 0 {
		// Printing several transactions
		fmt.Println("Printing transactions from 0 to 10:")
		fmt.Println()
		for i := 0; i < 10; i++ {
			tx := block.GetTxs()[i]

			examples.PrintTxWithEvents(tx)
		}
	}
}
