module gitlab.com/Blockdaemon/ubiquity/ubiquity-go-client/examples/ws-blocks

go 1.22

toolchain go1.22.7

require gitlab.com/Blockdaemon/ubiquity/ubiquity-go-client v0.0.0

require (
	github.com/gorilla/websocket v1.5.3 // indirect
	golang.org/x/oauth2 v0.24.0 // indirect
)

replace gitlab.com/Blockdaemon/ubiquity/ubiquity-go-client v0.0.0 => ../../
