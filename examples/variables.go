package examples

const (
	ProtocolBTC = "bitcoin"
	ProtocolETH = "ethereum"

	NetworkMainnet = "mainnet"
	NetworkTestnet = "testnet"
)
