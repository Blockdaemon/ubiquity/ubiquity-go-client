package main

import (
	"context"
	"fmt"
	"os"

	"gitlab.com/Blockdaemon/ubiquity/ubiquity-go-client/examples"
	ubiquity "gitlab.com/Blockdaemon/ubiquity/ubiquity-go-client/v1/pkg/client"
)

/*
*
Paginating over transactions.

Env variables:
 1. UBI_ACCESS_TOKEN - required, Ubiquity API Access Token
 2. UBI_ENDPOINT - optional, Ubiquity API custom endpoint (prod by default)
 3. UBI_PROTOCOL - optional, protocol e.g. ethereum (bitcoin by default)
*/
func main() {
	// Access token is required
	accessToken := os.Getenv("UBI_ACCESS_TOKEN")
	if accessToken == "" {
		panic(fmt.Errorf("env variable 'UBI_ACCESS_TOKEN' must be set"))
	}

	// You can *optionally* set a custom endpoint or it will use prod
	config := ubiquity.NewConfiguration()
	if endpoint := os.Getenv("UBI_ENDPOINT"); endpoint != "" {
		config.Servers = ubiquity.ServerConfigurations{
			{
				URL:         endpoint,
				Description: "Custom endpoint",
			},
		}
	}

	// Creating client
	apiClient := ubiquity.NewAPIClient(config)

	// Context and protocol
	ctx := context.WithValue(context.Background(), ubiquity.ContextAccessToken, accessToken)

	// Use ubiquity.ProtocolsAPI.GetProtocols(ctx _context.Context) to fetch all supported protocols
	// See examples/protocols-overview
	var pl string
	if pl = os.Getenv("UBI_PROTOCOL"); pl == "" {
		pl = "bitcoin"
	}

	// Paginating 10 times over transaction pages with limit 10
	var limit int32 = 10
	order := "desc"
	var continuation *string

	fmt.Println("Paginating 10 times over transaction pages with limit 10:")
	fmt.Println()

	for i := 0; i < 10; i++ {
		req := apiClient.TransactionsAPI.GetTxs(ctx, pl, "mainnet").PageSize(limit).Order(order)
		if continuation != nil {
			req = req.PageToken(*continuation)
		}

		page, resp, err := req.Execute()
		if err != nil {
			panic(fmt.Errorf("failed to paginate %s transactions: got status '%s' and error '%v'",
				pl, resp.Status, err))
		}
		fmt.Printf("Got %s transactions page with size %d:\n", pl, page.GetTotal())
		printTxsPage(page)

		if meta, ok := page.GetMetaOk(); !ok {
			break // If we hit end of the list sooner than the loop ends
		} else {
			continuation = meta.Paging.NextPageToken
		}

		fmt.Println()
	}
}

func printTxsPage(page ubiquity.TxPage) {
	for _, tx := range page.GetData() {
		examples.PrintTx(tx)
	}
}
