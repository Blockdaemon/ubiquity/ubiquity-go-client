/*
 * Blockdaemon REST API
 *
 * Blockdaemon REST API provides a RESTful and uniform way to access blockchain resources, with a rich and reusable model across multiple protocols/cryptocurrencies.  [Documentation](https://docs.blockdaemon.com/reference/rest-api-overview)  ### Currently supported protocols:  * algorand   * mainnet * avalanche    * mainnet-c/testnet-c * bitcoin   * mainnet/testnet * bitcoincash   * mainnet/testnet * dogecoin   * mainnet/testnet * ethereum   * mainnet/holesky/sepolia * fantom   * mainnet/testnet * litecoin   * mainnet/testnet * near   * mainnet * optimism   * mainnet * polkadot   * mainnet/westend * polygon   * mainnet/amoy * solana   * mainnet/testnet * stellar   * mainnet/testnet * tezos   * mainnet * tron   * mainnet/nile * xrp   * mainnet  ### Pagination Certain resources contain a lot of data, more than what's practical to return for a single request. With the help of pagination, the data is split across multiple responses. Each response returns a subset of the items requested, and a continuation token.  To get the next batch of items, copy the returned continuation token to the continuation query parameter and repeat the request with the new URL. In case no continuation token is returned, there is no more data available.
 *
 * API version: 3.0.0
 * Contact: support@blockdaemon.com
 */

// Code generated by OpenAPI Generator (https://openapi-generator.tech); DO NOT EDIT.

package ubiquity

import (
	"encoding/json"
)

// TxCreateProtocol Protocol specific parameters for transaction creation.
type TxCreateProtocol struct {
	Avalanche *TxCreateEvm `json:"avalanche,omitempty"`
	Ethereum  *TxCreateEvm `json:"ethereum,omitempty"`
	Fantom    *TxCreateEvm `json:"fantom,omitempty"`
	Polygon   *TxCreateEvm `json:"polygon,omitempty"`
}

// NewTxCreateProtocol instantiates a new TxCreateProtocol object
// This constructor will assign default values to properties that have it defined,
// and makes sure properties required by API are set, but the set of arguments
// will change when the set of required properties is changed
func NewTxCreateProtocol() *TxCreateProtocol {
	this := TxCreateProtocol{}
	return &this
}

// NewTxCreateProtocolWithDefaults instantiates a new TxCreateProtocol object
// This constructor will only assign default values to properties that have it defined,
// but it doesn't guarantee that properties required by API are set
func NewTxCreateProtocolWithDefaults() *TxCreateProtocol {
	this := TxCreateProtocol{}
	return &this
}

// GetAvalanche returns the Avalanche field value if set, zero value otherwise.
func (o *TxCreateProtocol) GetAvalanche() TxCreateEvm {
	if o == nil || o.Avalanche == nil {
		var ret TxCreateEvm
		return ret
	}
	return *o.Avalanche
}

// GetAvalancheOk returns a tuple with the Avalanche field value if set, nil otherwise
// and a boolean to check if the value has been set.
func (o *TxCreateProtocol) GetAvalancheOk() (*TxCreateEvm, bool) {
	if o == nil || o.Avalanche == nil {
		return nil, false
	}
	return o.Avalanche, true
}

// HasAvalanche returns a boolean if a field has been set.
func (o *TxCreateProtocol) HasAvalanche() bool {
	if o != nil && o.Avalanche != nil {
		return true
	}

	return false
}

// SetAvalanche gets a reference to the given TxCreateEvm and assigns it to the Avalanche field.
func (o *TxCreateProtocol) SetAvalanche(v TxCreateEvm) {
	o.Avalanche = &v
}

// GetEthereum returns the Ethereum field value if set, zero value otherwise.
func (o *TxCreateProtocol) GetEthereum() TxCreateEvm {
	if o == nil || o.Ethereum == nil {
		var ret TxCreateEvm
		return ret
	}
	return *o.Ethereum
}

// GetEthereumOk returns a tuple with the Ethereum field value if set, nil otherwise
// and a boolean to check if the value has been set.
func (o *TxCreateProtocol) GetEthereumOk() (*TxCreateEvm, bool) {
	if o == nil || o.Ethereum == nil {
		return nil, false
	}
	return o.Ethereum, true
}

// HasEthereum returns a boolean if a field has been set.
func (o *TxCreateProtocol) HasEthereum() bool {
	if o != nil && o.Ethereum != nil {
		return true
	}

	return false
}

// SetEthereum gets a reference to the given TxCreateEvm and assigns it to the Ethereum field.
func (o *TxCreateProtocol) SetEthereum(v TxCreateEvm) {
	o.Ethereum = &v
}

// GetFantom returns the Fantom field value if set, zero value otherwise.
func (o *TxCreateProtocol) GetFantom() TxCreateEvm {
	if o == nil || o.Fantom == nil {
		var ret TxCreateEvm
		return ret
	}
	return *o.Fantom
}

// GetFantomOk returns a tuple with the Fantom field value if set, nil otherwise
// and a boolean to check if the value has been set.
func (o *TxCreateProtocol) GetFantomOk() (*TxCreateEvm, bool) {
	if o == nil || o.Fantom == nil {
		return nil, false
	}
	return o.Fantom, true
}

// HasFantom returns a boolean if a field has been set.
func (o *TxCreateProtocol) HasFantom() bool {
	if o != nil && o.Fantom != nil {
		return true
	}

	return false
}

// SetFantom gets a reference to the given TxCreateEvm and assigns it to the Fantom field.
func (o *TxCreateProtocol) SetFantom(v TxCreateEvm) {
	o.Fantom = &v
}

// GetPolygon returns the Polygon field value if set, zero value otherwise.
func (o *TxCreateProtocol) GetPolygon() TxCreateEvm {
	if o == nil || o.Polygon == nil {
		var ret TxCreateEvm
		return ret
	}
	return *o.Polygon
}

// GetPolygonOk returns a tuple with the Polygon field value if set, nil otherwise
// and a boolean to check if the value has been set.
func (o *TxCreateProtocol) GetPolygonOk() (*TxCreateEvm, bool) {
	if o == nil || o.Polygon == nil {
		return nil, false
	}
	return o.Polygon, true
}

// HasPolygon returns a boolean if a field has been set.
func (o *TxCreateProtocol) HasPolygon() bool {
	if o != nil && o.Polygon != nil {
		return true
	}

	return false
}

// SetPolygon gets a reference to the given TxCreateEvm and assigns it to the Polygon field.
func (o *TxCreateProtocol) SetPolygon(v TxCreateEvm) {
	o.Polygon = &v
}

func (o TxCreateProtocol) MarshalJSON() ([]byte, error) {
	toSerialize := map[string]interface{}{}
	if o.Avalanche != nil {
		toSerialize["avalanche"] = o.Avalanche
	}
	if o.Ethereum != nil {
		toSerialize["ethereum"] = o.Ethereum
	}
	if o.Fantom != nil {
		toSerialize["fantom"] = o.Fantom
	}
	if o.Polygon != nil {
		toSerialize["polygon"] = o.Polygon
	}
	return json.Marshal(toSerialize)
}

type NullableTxCreateProtocol struct {
	value *TxCreateProtocol
	isSet bool
}

func (v NullableTxCreateProtocol) Get() *TxCreateProtocol {
	return v.value
}

func (v *NullableTxCreateProtocol) Set(val *TxCreateProtocol) {
	v.value = val
	v.isSet = true
}

func (v NullableTxCreateProtocol) IsSet() bool {
	return v.isSet
}

func (v *NullableTxCreateProtocol) Unset() {
	v.value = nil
	v.isSet = false
}

func NewNullableTxCreateProtocol(val *TxCreateProtocol) *NullableTxCreateProtocol {
	return &NullableTxCreateProtocol{value: val, isSet: true}
}

func (v NullableTxCreateProtocol) MarshalJSON() ([]byte, error) {
	return json.Marshal(v.value)
}

func (v *NullableTxCreateProtocol) UnmarshalJSON(src []byte) error {
	v.isSet = true
	return json.Unmarshal(src, &v.value)
}
