/*
 * Blockdaemon REST API
 *
 * Blockdaemon REST API provides a RESTful and uniform way to access blockchain resources, with a rich and reusable model across multiple protocols/cryptocurrencies.  [Documentation](https://docs.blockdaemon.com/reference/rest-api-overview)  ### Currently supported protocols:  * algorand   * mainnet * avalanche    * mainnet-c/testnet-c * bitcoin   * mainnet/testnet * bitcoincash   * mainnet/testnet * dogecoin   * mainnet/testnet * ethereum   * mainnet/holesky/sepolia * fantom   * mainnet/testnet * litecoin   * mainnet/testnet * near   * mainnet * optimism   * mainnet * polkadot   * mainnet/westend * polygon   * mainnet/amoy * solana   * mainnet/testnet * stellar   * mainnet/testnet * tezos   * mainnet * tron   * mainnet/nile * xrp   * mainnet  ### Pagination Certain resources contain a lot of data, more than what's practical to return for a single request. With the help of pagination, the data is split across multiple responses. Each response returns a subset of the items requested, and a continuation token.  To get the next batch of items, copy the returned continuation token to the continuation query parameter and repeat the request with the new URL. In case no continuation token is returned, there is no more data available.
 *
 * API version: 3.0.0
 * Contact: support@blockdaemon.com
 */

// Code generated by OpenAPI Generator (https://openapi-generator.tech); DO NOT EDIT.

package ubiquity

import (
	"encoding/json"
	"fmt"
)

// FeeEstimateResponse struct for FeeEstimateResponse
type FeeEstimateResponse struct {
	EvmFeeEstimate *EvmFeeEstimate
	FeeEstimate    *FeeEstimate
}

// Unmarshal JSON data into any of the pointers in the struct
func (dst *FeeEstimateResponse) UnmarshalJSON(data []byte) error {
	var err error
	// try to unmarshal JSON data into EvmFeeEstimate
	err = json.Unmarshal(data, &dst.EvmFeeEstimate)
	if err == nil {
		jsonEvmFeeEstimate, _ := json.Marshal(dst.EvmFeeEstimate)
		if string(jsonEvmFeeEstimate) == "{}" { // empty struct
			dst.EvmFeeEstimate = nil
		} else {
			return nil // data stored in dst.EvmFeeEstimate, return on the first match
		}
	} else {
		dst.EvmFeeEstimate = nil
	}

	// try to unmarshal JSON data into FeeEstimate
	err = json.Unmarshal(data, &dst.FeeEstimate)
	if err == nil {
		jsonFeeEstimate, _ := json.Marshal(dst.FeeEstimate)
		if string(jsonFeeEstimate) == "{}" { // empty struct
			dst.FeeEstimate = nil
		} else {
			return nil // data stored in dst.FeeEstimate, return on the first match
		}
	} else {
		dst.FeeEstimate = nil
	}

	return fmt.Errorf("Data failed to match schemas in anyOf(FeeEstimateResponse)")
}

// Marshal data from the first non-nil pointers in the struct to JSON
func (src *FeeEstimateResponse) MarshalJSON() ([]byte, error) {
	if src.EvmFeeEstimate != nil {
		return json.Marshal(&src.EvmFeeEstimate)
	}

	if src.FeeEstimate != nil {
		return json.Marshal(&src.FeeEstimate)
	}

	return nil, nil // no data in anyOf schemas
}

type NullableFeeEstimateResponse struct {
	value *FeeEstimateResponse
	isSet bool
}

func (v NullableFeeEstimateResponse) Get() *FeeEstimateResponse {
	return v.value
}

func (v *NullableFeeEstimateResponse) Set(val *FeeEstimateResponse) {
	v.value = val
	v.isSet = true
}

func (v NullableFeeEstimateResponse) IsSet() bool {
	return v.isSet
}

func (v *NullableFeeEstimateResponse) Unset() {
	v.value = nil
	v.isSet = false
}

func NewNullableFeeEstimateResponse(val *FeeEstimateResponse) *NullableFeeEstimateResponse {
	return &NullableFeeEstimateResponse{value: val, isSet: true}
}

func (v NullableFeeEstimateResponse) MarshalJSON() ([]byte, error) {
	return json.Marshal(v.value)
}

func (v *NullableFeeEstimateResponse) UnmarshalJSON(src []byte) error {
	v.isSet = true
	return json.Unmarshal(src, &v.value)
}
