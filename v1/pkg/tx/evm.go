package ubiquitytx

import (
	"context"
	"crypto/ecdsa"
	"encoding/hex"
	"fmt"
	"math/big"
	"strings"

	"github.com/ethereum/go-ethereum/core/types"
	"github.com/ethereum/go-ethereum/crypto"
	ubiquity "gitlab.com/Blockdaemon/ubiquity/ubiquity-go-client/v1/pkg/client"
)

type FeeEstimationType uint8

const (
	UbiquityFeeEstimationNone FeeEstimationType = iota
	UbiquityFeeEstimationSlow
	UbiquityFeeEstimationMedium
	UbiquityFeeEstimationFast
)

// EVMTransaction is a generic interface to reflect a Ethereum Virtual Machine
// compatbile transaction. All specific transaction types for EVM based chains
// implement this interface.
type EVMTransaction interface {
	GetProtocol() string
	GetNetwork() string
	GetPrivateKey() string
	GetRecipient() string
	GetAmount() *big.Float
	GetNonce() *int64
	GetMaxPriorityFeePerGas() *big.Int
	GetMaxFeePerGas() *big.Int
	GetGas() *big.Int
	GetFeeEstimationType() FeeEstimationType

	SetTxProtocolParams(i *ubiquity.TxCreateEvm) *ubiquity.TxCreateProtocol
}

func ValidateEVMTransaction(e EVMTransaction) error {
	if strings.TrimSpace(e.GetProtocol()) == "" {
		return fmt.Errorf("field 'Protocol' is required")
	}
	if strings.TrimSpace(e.GetNetwork()) == "" {
		return fmt.Errorf("field 'Network' is required")
	}
	if strings.TrimSpace(e.GetPrivateKey()) == "" {
		return fmt.Errorf("field 'PrivateKey' is required")
	}
	if strings.TrimSpace(e.GetRecipient()) == "" {
		return fmt.Errorf("field 'To' is required")
	}
	if e.GetAmount() == nil {
		return fmt.Errorf("field 'Amount' is required")
	}
	return nil
}

// GetPrivateKeyEDCSA retrieves the private key from EVMTransaction
func GetPrivateKeyECDSA(e EVMTransaction) (*ecdsa.PrivateKey, error) {
	privKey, err := crypto.HexToECDSA(e.GetPrivateKey())
	if err != nil {
		return nil, fmt.Errorf("failed to parse private key: %v", err)
	}

	return privKey, nil
}

// EstimateEVMTransactionFee uses the Ubiquity /tx/estimate_fee API to estimate the fee of an EVMTransaction.
// Uses the tx.GetFeeEstimationType() to select the category. Default is UbiquityFeeEstimationMedium.
func (s UbiquityTransactionService) EstimateEVMTransactionFee(ctx context.Context, tx EVMTransaction) (*big.Int, *big.Int, error) {
	feeResp, resp, err := s.feesAPI.GetFeeEstimate(ctx, tx.GetProtocol(), tx.GetNetwork()).Execute()
	if err != nil {
		return nil, nil, fmt.Errorf("ubiquity EstimateFee failure: resp. status = '%v' and err = '%v'",
			resp.Status,
			err,
		)
	}

	evmFee := feeResp.EstimatedFees.EvmFeeEstimate.Medium
	switch tx.GetFeeEstimationType() {
	case UbiquityFeeEstimationSlow:
		evmFee = feeResp.EstimatedFees.EvmFeeEstimate.Slow
	case UbiquityFeeEstimationFast:
		evmFee = feeResp.EstimatedFees.EvmFeeEstimate.Fast
	}
	return &evmFee.MaxTotalFee.Int, &evmFee.MaxPriorityFee.Int, nil
}

// CreateEVMTransaction creates a new transaction using the Ubiquity /tx/create endpoint using the EVMTransaction interface
// This function returns an unsigned transaction.
func (s UbiquityTransactionService) CreateEVMTransaction(ctx context.Context, tx EVMTransaction) (*ubiquity.UnsignedTx, error) {
	if err := ValidateEVMTransaction(tx); err != nil {
		return nil, fmt.Errorf("transaction validation failure: %v", err)
	}

	privKey, err := GetPrivateKeyECDSA(tx)
	if err != nil {
		return nil, err
	}

	pubKey, ok := privKey.Public().(*ecdsa.PublicKey)
	if !ok {
		return nil, fmt.Errorf("unexpected public key type: %T", pubKey)
	}

	from := crypto.PubkeyToAddress(*pubKey)
	var gas, maxFeePerGas, maxPriorityFeePerGas *int64
	if tx.GetGas() != nil {
		gas = toPointer(tx.GetGas().Int64())
	}

	if tx.GetMaxFeePerGas() != nil {
		maxFeePerGas = toPointer(tx.GetMaxFeePerGas().Int64())
	}

	if tx.GetMaxPriorityFeePerGas() != nil {
		maxPriorityFeePerGas = toPointer(tx.GetMaxPriorityFeePerGas().Int64())
	}

	if (tx.GetMaxPriorityFeePerGas() == nil || tx.GetMaxFeePerGas() == nil) && tx.GetFeeEstimationType() != UbiquityFeeEstimationNone {
		estimatedMaxFeePerGas, estimatedMaxPriorityFeePerGas, err := s.EstimateEVMTransactionFee(ctx, tx)
		if err != nil {
			return nil, fmt.Errorf("failed to estimate fees: %w", err)
		}

		maxFeePerGas = toPointer(estimatedMaxFeePerGas.Int64())
		maxPriorityFeePerGas = toPointer(estimatedMaxPriorityFeePerGas.Int64())
	}

	return s.TxCreate(ctx, tx.GetProtocol(), tx.GetNetwork(), ubiquity.TxCreate{
		From: from.Hex(),
		To: []ubiquity.TxDestination{{
			Destination: tx.GetRecipient(),
			Amount:      tx.GetAmount().String(),
		}},
		Index: tx.GetNonce(),
		Protocol: tx.SetTxProtocolParams(&ubiquity.TxCreateEvm{
			Gas:                  gas,
			MaxPriorityFeePerGas: maxPriorityFeePerGas,
			MaxFeePerGas:         maxFeePerGas,
		}),
	})
}

// SignEVMTransactionWithSigner signs an unsigned hex transaction using the provided private key and signer
// This will work for all Ethereum Virtual Machine (EVM) compliant blockchains, including Ethereum and Polygon
// This function allows more control. For higher level functions use 'SignEthereumTransaction' and 'SignPolygonTransaction'
func SignEVMTransactionWithSigner(unsignedHexTx string, privKey *ecdsa.PrivateKey, signer types.Signer) (txHash string, signedTx []byte, err error) {
	rawTx, err := hex.DecodeString(unsignedHexTx)
	if err != nil {
		return "", nil, fmt.Errorf("failed to decode hex unsigned tx: %w", err)
	}

	unsignedTx := new(types.Transaction)
	if err := unsignedTx.UnmarshalBinary(rawTx); err != nil {
		return "", nil, fmt.Errorf("faile to decode raw unsigned tx: %w", err)
	}

	signedRawTx, err := types.SignTx(unsignedTx, signer, privKey)
	if err != nil {
		return "", nil, fmt.Errorf("failed to sign: %v", err)
	}

	txHash = signedRawTx.Hash().Hex()
	signedTx, err = signedRawTx.MarshalBinary()
	return
}
