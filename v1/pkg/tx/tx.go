package ubiquitytx

import (
	"context"
	"fmt"
	"io"

	ubiquity "gitlab.com/Blockdaemon/ubiquity/ubiquity-go-client/v1/pkg/client"
)

const (
	networkTestnet = "testnet"
	networkMainnet = "mainnet"
)

type SendTxResult struct {
	TxHash string
	TxID   string
}

// UbiquityTransactionService provides API to send various protocol transactions.
// So far it supports BTC and ETH.
type UbiquityTransactionService struct {
	txAPI   ubiquity.TransactionsAPI
	feesAPI ubiquity.FeeEstimatorAPI
}

func NewService(txAPI ubiquity.TransactionsAPI, feesAPI ubiquity.FeeEstimatorAPI) *UbiquityTransactionService {
	return &UbiquityTransactionService{
		txAPI:   txAPI,
		feesAPI: feesAPI,
	}
}

// TxCreate creates a transaction using the Ubiquity /tx/create endpoint
func (s UbiquityTransactionService) TxCreate(ctx context.Context, protocol, network string, tx ubiquity.TxCreate) (*ubiquity.UnsignedTx, error) {
	txRes, resp, err := s.txAPI.
		TxCreate(ctx, protocol, network).
		TxCreate(tx).
		Execute()

	if err != nil && resp != nil {
		body, _ := io.ReadAll(resp.Body)
		defer resp.Body.Close()
		return nil, fmt.Errorf("ubiquity TxCreate failure: resp. status = '%v' and err = '%w' and body = '%s'", resp.Status, err, string(body))
	} else if err != nil {
		return nil, fmt.Errorf("ubiquity TxCreate failure: %w", err)
	}

	if resp != nil {
		defer resp.Body.Close()
	}

	return &txRes, nil
}

// TxSend sends a transaction using the Ubiquity /tx/send endpoint
func (s UbiquityTransactionService) TxSend(ctx context.Context, protocol, network string, signedTx string) (*ubiquity.TxReceipt, error) {
	txReceipt, resp, err := s.txAPI.TxSend(ctx, protocol, network).
		SignedTx(ubiquity.SignedTx{Tx: signedTx}).
		Execute()

	if err != nil && resp != nil {
		body, _ := io.ReadAll(resp.Body)
		defer resp.Body.Close()
		return nil, fmt.Errorf("ubiquity TxSend failure: resp. status = '%v' and err = '%w' and body = '%s'", resp.Status, err, string(body))
	} else if err != nil {
		return nil, fmt.Errorf("ubiquity TxSend failure: %w", err)
	}

	if resp != nil {
		defer resp.Body.Close()
	}

	return &txReceipt, nil
}

type TxInput struct {
	Source string // UTXO, input transaction id
	Index  uint32 // UTXO, input index
}

type TxOutput struct {
	Destination string // Destination address
	Amount      int64  // In sat.
}

func toPointer[T any](a T) *T {
	return &a
}
