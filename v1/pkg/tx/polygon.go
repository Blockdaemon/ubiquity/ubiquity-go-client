package ubiquitytx

import (
	"context"
	"crypto/ecdsa"
	"encoding/hex"
	"fmt"
	"math/big"

	"github.com/ethereum/go-ethereum/core/types"

	ubiquity "gitlab.com/Blockdaemon/ubiquity/ubiquity-go-client/v1/pkg/client"
)

const networkAmoy = "amoy"

var (
	polygonChains = map[string]*big.Int{
		networkMainnet: big.NewInt(137),
		networkAmoy:    big.NewInt(80002),
	}
)

var _ EVMTransaction = (*PolygonTransaction)(nil)

type PolygonTransaction struct {
	Network              string
	PrivateKey           string
	To                   string
	Amount               *big.Float // In Matic
	Nonce                *int64     // Current transaction count
	MaxPriorityFeePerGas *big.Int
	MaxFeePerGas         *big.Int
	Gas                  *big.Int

	// FeeEstimation can be used to indicate whether the Ubiquity
	// /tx/estimate_fee API should be used.
	// When MaxPriorityFeePerGas or MaxFeePerGas is set or when
	// this is set to UbiquityFeeEstimationNone (the default)
	// the fee estimation API is not used.
	FeeEstimation FeeEstimationType
}

func (f PolygonTransaction) GetNetwork() string                      { return f.Network }
func (f PolygonTransaction) GetProtocol() string                     { return "polygon" }
func (f PolygonTransaction) GetPrivateKey() string                   { return f.PrivateKey }
func (f PolygonTransaction) GetRecipient() string                    { return f.To }
func (f PolygonTransaction) GetAmount() *big.Float                   { return f.Amount }
func (f PolygonTransaction) GetNonce() *int64                        { return f.Nonce }
func (f PolygonTransaction) GetMaxPriorityFeePerGas() *big.Int       { return f.MaxPriorityFeePerGas }
func (f PolygonTransaction) GetMaxFeePerGas() *big.Int               { return f.MaxFeePerGas }
func (f PolygonTransaction) GetGas() *big.Int                        { return f.Gas }
func (f PolygonTransaction) GetFeeEstimationType() FeeEstimationType { return f.FeeEstimation }

func (f PolygonTransaction) SetTxProtocolParams(i *ubiquity.TxCreateEvm) *ubiquity.TxCreateProtocol {
	return &ubiquity.TxCreateProtocol{Polygon: i}
}

// SendMATIC creates, signs and sends MATIC transaction. Under the hood it uses Ubiquity TxCreate and TxSend API.
func (s UbiquityTransactionService) SendMATIC(ctx context.Context, tx *PolygonTransaction) (*SendTxResult, error) {
	txRes, err := s.CreateEVMTransaction(ctx, tx)
	if err != nil {
		return nil, err
	}

	privKey, err := GetPrivateKeyECDSA(tx)
	if err != nil {
		return nil, err
	}

	txHash, signedTxData, err := SignPolygonTransaction(txRes.UnsignedTx, tx.Network, privKey)
	if err != nil {
		return nil, fmt.Errorf("failed to marshal: %v", err)
	}

	txReceipt, err := s.TxSend(ctx, "polygon", tx.Network, hex.EncodeToString(signedTxData))
	if err != nil {
		return nil, err
	}

	return &SendTxResult{
		TxHash: txHash,
		TxID:   txReceipt.GetId(),
	}, nil
}

// SignPolygonTransaction signs an unsigned hex encoded transaction for the desired 'network'.
// This function signs a post London hard fork transaction, als referred to as an EIP1559 transaction.
func SignPolygonTransaction(unsignedHexTx string, network string, privKey *ecdsa.PrivateKey) (txHash string, signedTx []byte, err error) {
	chainID, ok := polygonChains[network]
	if !ok {
		return "", nil, fmt.Errorf("network %s is not supported", network)
	}

	signer := types.NewLondonSigner(chainID)
	return SignEVMTransactionWithSigner(unsignedHexTx, privKey, signer)
}
