package ubiquitytx

import (
	"context"
	"crypto/ecdsa"
	"encoding/hex"
	"fmt"
	"math/big"

	"github.com/ethereum/go-ethereum/core/types"
	ubiquity "gitlab.com/Blockdaemon/ubiquity/ubiquity-go-client/v1/pkg/client"
)

var (
	fantomChains = map[string]*big.Int{
		networkMainnet: big.NewInt(250),
		networkTestnet: big.NewInt(4002),
	}
)

var _ EVMTransaction = (*FantomTransaction)(nil)

type FantomTransaction struct {
	Network              string
	PrivateKey           string
	To                   string
	Amount               *big.Float // In FTM
	Nonce                *int64     // Current transaction count
	MaxPriorityFeePerGas *big.Int
	MaxFeePerGas         *big.Int
	Gas                  *big.Int

	// FeeEstimation can be used to indicate whether the Ubiquity
	// /tx/estimate_fee API should be used.
	// When MaxPriorityFeePerGas or MaxFeePerGas is set or when
	// this is set to UbiquityFeeEstimationNone (the default)
	// the fee estimation API is not used.
	FeeEstimation FeeEstimationType
}

func (f FantomTransaction) GetNetwork() string                      { return f.Network }
func (f FantomTransaction) GetProtocol() string                     { return "fantom" }
func (f FantomTransaction) GetPrivateKey() string                   { return f.PrivateKey }
func (f FantomTransaction) GetRecipient() string                    { return f.To }
func (f FantomTransaction) GetAmount() *big.Float                   { return f.Amount }
func (f FantomTransaction) GetNonce() *int64                        { return f.Nonce }
func (f FantomTransaction) GetMaxPriorityFeePerGas() *big.Int       { return f.MaxPriorityFeePerGas }
func (f FantomTransaction) GetMaxFeePerGas() *big.Int               { return f.MaxFeePerGas }
func (f FantomTransaction) GetGas() *big.Int                        { return f.Gas }
func (f FantomTransaction) GetFeeEstimationType() FeeEstimationType { return f.FeeEstimation }

func (f FantomTransaction) SetTxProtocolParams(i *ubiquity.TxCreateEvm) *ubiquity.TxCreateProtocol {
	return &ubiquity.TxCreateProtocol{Fantom: i}
}

// SendFTM creates, signs and sends FTM transaction. Under the hood it uses Ubiquity TxCreate and TxSend API.
func (s UbiquityTransactionService) SendFTM(ctx context.Context, tx *FantomTransaction) (*SendTxResult, error) {
	txRes, err := s.CreateEVMTransaction(ctx, tx)
	if err != nil {
		return nil, err
	}

	privKey, err := GetPrivateKeyECDSA(tx)
	if err != nil {
		return nil, err
	}

	txHash, signedTxData, err := SignFantomTransaction(txRes.UnsignedTx, tx.Network, privKey)
	if err != nil {
		return nil, fmt.Errorf("failed to marshal: %v", err)
	}

	txReceipt, err := s.TxSend(ctx, "fantom", tx.Network, hex.EncodeToString(signedTxData))
	if err != nil {
		return nil, err
	}

	return &SendTxResult{
		TxHash: txHash,
		TxID:   txReceipt.GetId(),
	}, nil
}

// SignFantomTransaction signs an unsigned hex encoded transaction for the desired 'network'.
// This function signs a post London hard fork transaction, als referred to as an EIP1559 transaction.
func SignFantomTransaction(unsignedHexTx string, network string, privKey *ecdsa.PrivateKey) (txHash string, signedTx []byte, err error) {
	chainID, ok := fantomChains[network]
	if !ok {
		return "", nil, fmt.Errorf("network %s is not supported", network)
	}

	signer := types.NewLondonSigner(chainID)
	return SignEVMTransactionWithSigner(unsignedHexTx, privKey, signer)
}
