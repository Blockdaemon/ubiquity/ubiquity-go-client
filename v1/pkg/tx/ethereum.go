package ubiquitytx

import (
	"context"
	"crypto/ecdsa"
	"encoding/hex"
	"fmt"
	"math/big"
	"strings"

	"github.com/ethereum/go-ethereum/core/types"

	ubiquity "gitlab.com/Blockdaemon/ubiquity/ubiquity-go-client/v1/pkg/client"
)

const (
	ethereumGoerli     = "goerli"
	ethereumHolesky    = "holesky"
	ethereumSepolia    = "sepolia"
	ethereumTxGasLimit = 21000 // Default for simple transfer transaction
)

var (
	ethereumChains = map[string]*big.Int{
		networkMainnet:  big.NewInt(1),
		ethereumGoerli:  big.NewInt(5),
		ethereumHolesky: big.NewInt(17000),
		ethereumSepolia: big.NewInt(11155111),
	}
)

var _ EVMTransaction = (*EthereumTransaction)(nil)

type EthereumTransaction struct {
	Network              string
	PrivateKey           string
	To                   string
	Amount               *big.Float // In Eth
	Nonce                *int64     // Current transaction count
	Gas                  *big.Int
	MaxPriorityFeePerGas *big.Int
	MaxFeePerGas         *big.Int

	// FeeEstimation can be used to indicate whether the Ubiquity
	// /tx/estimate_fee API should be used. Be default uses the
	// medium category.
	// When MaxPriorityFeePerGas or MaxFeePerGas is set
	// the fee estimation API is not used.
	FeeEstimation *FeeEstimationType
}

func (f EthereumTransaction) GetNetwork() string                { return f.Network }
func (f EthereumTransaction) GetProtocol() string               { return "ethereum" }
func (f EthereumTransaction) GetPrivateKey() string             { return f.PrivateKey }
func (f EthereumTransaction) GetRecipient() string              { return f.To }
func (f EthereumTransaction) GetAmount() *big.Float             { return f.Amount }
func (f EthereumTransaction) GetNonce() *int64                  { return f.Nonce }
func (f EthereumTransaction) GetMaxPriorityFeePerGas() *big.Int { return f.MaxPriorityFeePerGas }
func (f EthereumTransaction) GetMaxFeePerGas() *big.Int         { return f.MaxFeePerGas }
func (f EthereumTransaction) GetGas() *big.Int                  { return f.Gas }

func (f EthereumTransaction) GetFeeEstimationType() FeeEstimationType {
	if f.FeeEstimation != nil {
		return *f.FeeEstimation
	}
	return UbiquityFeeEstimationMedium
}

func (f EthereumTransaction) SetTxProtocolParams(i *ubiquity.TxCreateEvm) *ubiquity.TxCreateProtocol {
	return &ubiquity.TxCreateProtocol{Ethereum: i}
}

func (t EthereumTransaction) validate() error {
	if strings.TrimSpace(t.Network) == "" {
		return fmt.Errorf("field 'Network' is required")
	}
	if strings.TrimSpace(t.PrivateKey) == "" {
		return fmt.Errorf("field 'PrivateKey' is required")
	}
	if strings.TrimSpace(t.To) == "" {
		return fmt.Errorf("field 'To' is required")
	}
	if t.Amount == nil {
		return fmt.Errorf("field 'Amount' is required")
	}
	return nil
}

// SendETH creates, signs and sends ETH transaction. Under the hood it uses Ubiquity TxSend API.
func (s UbiquityTransactionService) SendETH(ctx context.Context, tx *EthereumTransaction) (*SendTxResult, error) {
	txRes, err := s.CreateEVMTransaction(ctx, tx)
	if err != nil {
		return nil, err
	}

	privKey, err := GetPrivateKeyECDSA(tx)
	if err != nil {
		return nil, err
	}

	txHash, signedTxData, err := SignEthereumTransaction(txRes.UnsignedTx, tx.Network, privKey)
	if err != nil {
		return nil, fmt.Errorf("failed to marshal: %v", err)
	}

	txReceipt, err := s.TxSend(ctx, "ethereum", tx.Network, hex.EncodeToString(signedTxData))
	if err != nil {
		return nil, err
	}

	return &SendTxResult{
		TxHash: txHash,
		TxID:   txReceipt.GetId(),
	}, nil
}

// SignEthereumTransaction signs an unsigned hex encoded transaction for the desired 'network'.
// This function signs a post London hard fork transaction, als referred to as an EIP1559 transaction.
func SignEthereumTransaction(unsignedHexTx string, network string, privKey *ecdsa.PrivateKey) (txHash string, signedTx []byte, err error) {
	chainID, ok := ethereumChains[network]
	if !ok {
		return "", nil, fmt.Errorf("network %s is not supported", network)
	}

	signer := types.NewLondonSigner(chainID)
	return SignEVMTransactionWithSigner(unsignedHexTx, privKey, signer)
}

// SignEthereumLegacyTransaction signs an unsigned hex encoded transaction for the desired 'network'.
// This function signs a legacy transaction, these are transactions commonly used before the London hard fork
func SignEthereumLegacyTransaction(unsignedHexTx string, network string, privKey *ecdsa.PrivateKey) (txHash string, signedTx []byte, err error) {
	chainID, ok := ethereumChains[network]
	if !ok {
		return "", nil, fmt.Errorf("network %s is not supported", network)
	}

	signer := types.NewEIP2930Signer(chainID)
	return SignEVMTransactionWithSigner(unsignedHexTx, privKey, signer)
}
