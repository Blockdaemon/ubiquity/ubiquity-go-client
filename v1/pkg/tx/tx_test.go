package ubiquitytx

import (
	"context"
	"encoding/json"
	"fmt"
	"math/big"
	"net/http"
	"net/http/httptest"
	"testing"

	"github.com/stretchr/testify/mock"
	"github.com/test-go/testify/assert"

	ubiquity "gitlab.com/Blockdaemon/ubiquity/ubiquity-go-client/v1/pkg/client"
	"gitlab.com/Blockdaemon/ubiquity/ubiquity-go-client/v1/pkg/tx/mocks"
)

func TestBitcoinTransaction_validate(t *testing.T) {
	testCases := []BitcoinTransaction{
		{
			From:       []TxInput{{Source: "abc", Index: 0}},
			To:         []TxOutput{{Destination: "def", Amount: 12345}},
			PrivateKey: "<private key>",
		},
		{
			Network:    "testnet",
			To:         []TxOutput{{Destination: "def", Amount: 12345}},
			PrivateKey: "<private key>",
		},
		{
			Network:    "testnet",
			From:       []TxInput{{Source: "abc", Index: 0}},
			PrivateKey: "<private key>",
		},
		{
			Network: "testnet",
			From:    []TxInput{{Source: "abc", Index: 0}},
			To:      []TxOutput{{Destination: "def", Amount: 12345}},
		},
		{
			Network:    "testnet",
			From:       []TxInput{{}},
			To:         []TxOutput{{Destination: "def", Amount: 12345}},
			PrivateKey: "<private key>",
		},
		{
			Network:    "testnet",
			From:       []TxInput{{Source: "abc", Index: 0}},
			To:         []TxOutput{{}},
			PrivateKey: "<private key>",
		},
	}

	for _, tc := range testCases {
		t.Run(fmt.Sprintf("%v", tc), func(t *testing.T) {
			assert.Error(t, tc.validate())
		})
	}
}

func TestBitcoinTransaction_newDate(t *testing.T) {
	txAPIMock := new(mocks.TransactionsAPI)

	ctx := context.Background()
	protocol := "bitcoin"
	network := "testnet"
	index := int32(0)
	id := "txID"
	date := int64(1724808601898)
	blockID := "blockID"
	blockNum := int32(2892788)
	confirmations := int32(3827)
	truth := true

	outResp := ubiquity.TxOutputResponse{
		Index:         &index,
		TxId:          &id,
		Date:          &date,
		BlockId:       &blockID,
		BlockNumber:   &blockNum,
		Confirmations: &confirmations,
	}

	outputs := []ubiquity.TxOutputsData{{
		Status:  nil,
		IsSpent: &truth,
		Spent:   &outResp,
		Value:   nil,
		Mined:   nil,
	}}

	txOuts := ubiquity.TxOutputs{
		Data: &outputs,
	}

	request := ubiquity.ApiGetUTXOByAccountRequest{ApiService: txAPIMock}
	txAPIMock.On("GetUTXOByAccount", ctx, protocol, network, "address").Return(request)
	txAPIMock.On("GetUTXOByAccountExecute", mock.AnythingOfType("ubiquity.ApiGetUTXOByAccountRequest")).
		Return(txOuts, &http.Response{StatusCode: 200}, nil)

	txService := NewService(txAPIMock, nil)
	req := txService.txAPI.GetUTXOByAccount(ctx, protocol, network, "address")
	resp, httpResp, err := txService.txAPI.GetUTXOByAccountExecute(req)

	assert.NoError(t, err)
	assert.Equal(t, 200, httpResp.StatusCode)
	assert.Equal(t, len(resp.GetData()), len(outputs))
	assert.True(t, len(resp.GetData()) > 0)
	assert.Equal(t, *resp.GetData()[0].Spent.Date, *outputs[0].Spent.Date)
}

func TestUbiquityTransactionService_SendBTCTestnet(t *testing.T) {
	txAPIMock := new(mocks.TransactionsAPI)

	ctx := context.Background()
	network := "testnet"
	txSendRequest := ubiquity.ApiTxSendRequest{ApiService: txAPIMock}
	txAPIMock.On("TxSend", ctx, "bitcoin", network).Return(txSendRequest)
	newTxID := "<new tx id>"
	txAPIMock.On("TxSendExecute", mock.AnythingOfType("ubiquity.ApiTxSendRequest")).
		Return(ubiquity.TxReceipt{Id: newTxID}, nil, nil)

	txService := NewService(txAPIMock, nil)

	r, err := txService.SendBTC(ctx, &BitcoinTransaction{
		Network: network,
		From:    []TxInput{{Source: "3c7be4102cefc284ef36237a09a3facaa16cec0ffa7eab36302dbf1294734e09", Index: 1}},
		To: []TxOutput{
			{Destination: "mqbGHFBYtsHo5rWoPdCgjoJcVPig96UWv8", Amount: 1000}, // Testnet addresses
			{Destination: "n17f2LuLKddqWj59a4wUc8Ar7iakcPdsEo", Amount: 8000},
		},
		PrivateKey: "P9Z49xp3dXnY5gvbjr2Wc3bDv2U7JovE75qbjJfGJFicek7tFAsX",
	})

	assert.NoError(t, err)
	assert.Equal(t, newTxID, r.TxID)
	assert.NotEmpty(t, r.TxHash)

	txAPIMock.AssertExpectations(t)
}

func TestUbiquityTransactionService_SendBTCMainnet(t *testing.T) {
	txAPIMock := new(mocks.TransactionsAPI)

	ctx := context.Background()
	network := "mainnet"
	txSendRequest := ubiquity.ApiTxSendRequest{ApiService: txAPIMock}
	txAPIMock.On("TxSend", ctx, "bitcoin", network).Return(txSendRequest)
	newTxID := "<new tx id>"
	txAPIMock.On("TxSendExecute", mock.AnythingOfType("ubiquity.ApiTxSendRequest")).
		Return(ubiquity.TxReceipt{Id: newTxID}, nil, nil)

	txService := NewService(txAPIMock, nil)

	r, err := txService.SendBTC(ctx, &BitcoinTransaction{
		Network: network,
		From:    []TxInput{{Source: "782ad4d316fa75262bc70be944ca23a39c4b41e5f83368dc67d176dddef169ba", Index: 1}},
		To: []TxOutput{
			{Destination: "35DjpnyX3MySbHbCN4PYq9qnKbDq8sN4Ka", Amount: 1000}, // Mainnet addresses
			{Destination: "1Kr6QSydW9bFQG1mXiPNNu6WpJGmUa9i1g", Amount: 8000},
		},
		PrivateKey: "P9Z49xp3dXnY5gvbjr2Wc3bDv2U7JovE75qbjJfGJFicek7tFAsX", // We can use same private key
	})

	assert.NoError(t, err)
	assert.Equal(t, newTxID, r.TxID)
	assert.NotEmpty(t, r.TxHash)

	txAPIMock.AssertExpectations(t)
}

func TestEthereumTransaction_validate(t *testing.T) {
	var nonce int64 = 1

	testCases := []EthereumTransaction{
		{
			PrivateKey: "<private key>",
			To:         "abc",
			Amount:     big.NewFloat(1.2345),
			Nonce:      &nonce,
		},
		{
			Network: "testnet",
			To:      "abc",
			Amount:  big.NewFloat(1.2345),
			Nonce:   &nonce,
		},
		{
			Network:    "testnet",
			PrivateKey: "<private key>",
			Amount:     big.NewFloat(1.2345),
			Nonce:      &nonce,
		},
		{
			Network:    "testnet",
			PrivateKey: "<private key>",
			To:         "abc",
			Nonce:      &nonce,
		},
	}

	for _, tc := range testCases {
		t.Run(fmt.Sprintf("%v", tc), func(t *testing.T) {
			assert.Error(t, tc.validate())
		})
	}
}

func TestUbiquityTransactionService_SendETH(t *testing.T) {
	var x ubiquity.EvmFee
	_ = json.Unmarshal([]byte(`{"max_total_fee":1234, "max_priority_fee":12345}`), &x)

	testCases := []struct {
		name, network                      string
		maxFeePerGas, maxPriorityFeePerGas *big.Int
		estimatedFee                       *ubiquity.EvmFeeEstimate
	}{
		{
			name:                 "MainnetWithFees",
			network:              "mainnet",
			maxFeePerGas:         new(big.Int),
			maxPriorityFeePerGas: new(big.Int),
		},
		{
			name:    "MainnetWithoutFeesShouldEstimate",
			network: "mainnet",
			estimatedFee: &ubiquity.EvmFeeEstimate{
				Medium: &x,
			},
		},
	}

	for _, tc := range testCases {
		t.Run(tc.name, func(t *testing.T) {
			testSendETH(t, tc.network, tc.maxFeePerGas, tc.maxPriorityFeePerGas, tc.estimatedFee)
		})
	}
}

func testSendETH(t *testing.T, network string, maxFeePerGas, maxPriorityFeePerGas *big.Int, estimatedFee *ubiquity.EvmFeeEstimate) {
	txAPIMock := new(mocks.TransactionsAPI)
	feesAPIMock := new(mocks.FeeEstimatorAPI)

	ctx := context.Background()
	newTxID := "<new tx id>"

	txCreateRequest := ubiquity.ApiTxCreateRequest{ApiService: txAPIMock}
	txSendRequest := ubiquity.ApiTxSendRequest{ApiService: txAPIMock}
	feeEstimateRequest := ubiquity.ApiGetFeeEstimateRequest{ApiService: feesAPIMock}
	feesAPIMock.On("GetFeeEstimate", ctx, "ethereum", network).Return(feeEstimateRequest)
	feesAPIMock.On("GetFeeEstimateExecute", mock.AnythingOfType("ubiquity.ApiGetFeeEstimateRequest")).
		Return(ubiquity.InlineResponse200{
			EstimatedFees: &ubiquity.FeeEstimateResponse{
				EvmFeeEstimate: estimatedFee,
			},
		}, nil, nil)
	txAPIMock.On("TxCreate", ctx, "ethereum", network).Return(txCreateRequest)
	txAPIMock.On("TxCreateExecute", mock.AnythingOfType("ubiquity.ApiTxCreateRequest")).
		Return(ubiquity.UnsignedTx{UnsignedTx: "02e880038203e88207d082520894e40873f0a7231c301e08e2703868721c10e955c28203e880c0808080"}, nil, nil)
	txAPIMock.On("TxSend", ctx, "ethereum", network).Return(txSendRequest)
	txAPIMock.On("TxSendExecute", mock.AnythingOfType("ubiquity.ApiTxSendRequest")).
		Return(ubiquity.TxReceipt{Id: newTxID}, nil, nil)

	txService := NewService(txAPIMock, feesAPIMock)

	r, err := txService.SendETH(ctx, &EthereumTransaction{
		Network:              network,
		PrivateKey:           "56f8b9c2469e3d1fec30fa7e0623ca2c48e861bb9384977178c6a72bdde38cd0",
		To:                   "0x47128c68cF64a0aff7b436909e1Bd9A46168c93C",
		Amount:               big.NewFloat(0000.1),
		MaxFeePerGas:         maxFeePerGas,
		MaxPriorityFeePerGas: maxPriorityFeePerGas,
	})

	assert.NoError(t, err)
	assert.Equal(t, newTxID, r.TxID)
	assert.NotEmpty(t, r.TxHash)

	txAPIMock.AssertExpectations(t)
}

func TestEstimateFeeEVM(t *testing.T) {
	server := httptest.NewServer(http.HandlerFunc(func(rw http.ResponseWriter, req *http.Request) {
		rw.Header().Set("Content-Type", "application/json")
		rw.Write([]byte(`
			{
				"most_recent_block": 17922434,
				"estimated_fees": {
					"fast": {
						"max_priority_fee": 1500000000,
						"max_total_fee": 77946555841
					},
					"medium": {
						"max_priority_fee": 1200000000,
						"max_total_fee": 68914371989
					},
					"slow": {
						"max_priority_fee": 1000000000,
						"max_total_fee": 66811237945
					}
				}
			}
		`))
	}))

	cfg := ubiquity.NewConfiguration()
	cfg.Servers = ubiquity.ServerConfigurations{{
		URL: server.URL,
	}}
	apiClient := ubiquity.NewAPIClient(cfg)
	response, _, err := apiClient.FeeEstimatorAPI.GetFeeEstimate(context.TODO(), "ethereum", "mainnet").Execute()
	assert.NoError(t, err)

	assert.NotNil(t, response.EstimatedFees.EvmFeeEstimate)
	assert.NotNil(t, response.EstimatedFees.EvmFeeEstimate.Medium)
	assert.Equal(t, response.EstimatedFees.EvmFeeEstimate.Medium.MaxTotalFee.String(), "68914371989")
	assert.Equal(t, response.EstimatedFees.EvmFeeEstimate.Medium.MaxPriorityFee.String(), "1200000000")
}

func TestEstimateFee(t *testing.T) {
	server := httptest.NewServer(http.HandlerFunc(func(rw http.ResponseWriter, req *http.Request) {
		rw.Header().Set("Content-Type", "application/json")
		rw.Write([]byte(`
			{
				"most_recent_block": 17922434,
				"estimated_fees": {
					"fast":77946555841,
					"medium":68914371989,
					"slow":66811237945
				}
			}
		`))
	}))

	cfg := ubiquity.NewConfiguration()
	cfg.Servers = ubiquity.ServerConfigurations{{
		URL: server.URL,
	}}
	apiClient := ubiquity.NewAPIClient(cfg)
	response, _, err := apiClient.FeeEstimatorAPI.GetFeeEstimate(context.TODO(), "ethereum", "mainnet").Execute()
	assert.NoError(t, err)

	assert.NotNil(t, response.EstimatedFees.FeeEstimate)
	assert.NotNil(t, response.EstimatedFees.FeeEstimate.Medium)
	assert.Equal(t, response.EstimatedFees.FeeEstimate.Medium.String(), "68914371989")
}

func TestTxCreate(t *testing.T) {
	t.Parallel()

	txAPIMock := new(mocks.TransactionsAPI)
	txCreateRequest := ubiquity.ApiTxCreateRequest{ApiService: txAPIMock}

	txAPIMock.EXPECT().TxCreate(mock.Anything, "ethereum", "mainnet").Return(txCreateRequest)
	txAPIMock.EXPECT().TxCreateExecute(mock.AnythingOfType("ubiquity.ApiTxCreateRequest")).Return(ubiquity.UnsignedTx{UnsignedTx: "unsigned-tx"}, nil, nil)

	api := NewService(txAPIMock, nil)
	resp, err := api.TxCreate(context.TODO(), "ethereum", "mainnet", ubiquity.TxCreate{From: "unit-test"})
	assert.NoError(t, err)
	assert.Equal(t, "unsigned-tx", resp.UnsignedTx)
}

func TestTxSend(t *testing.T) {
	t.Parallel()

	txAPIMock := new(mocks.TransactionsAPI)
	txSendRequest := ubiquity.ApiTxSendRequest{ApiService: txAPIMock}

	txAPIMock.EXPECT().TxSend(mock.Anything, "ethereum", "mainnet").Return(txSendRequest)
	txAPIMock.EXPECT().TxSendExecute(mock.AnythingOfType("ubiquity.ApiTxSendRequest")).Return(ubiquity.TxReceipt{Id: "unit-test-passed"}, nil, nil)

	api := NewService(txAPIMock, nil)
	resp, err := api.TxSend(context.TODO(), "ethereum", "mainnet", "signed-raw-tx-string")
	assert.NoError(t, err)
	assert.Equal(t, "unit-test-passed", resp.Id)
}
