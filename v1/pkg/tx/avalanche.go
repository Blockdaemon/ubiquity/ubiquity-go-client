package ubiquitytx

import (
	"context"
	"crypto/ecdsa"
	"encoding/hex"
	"fmt"
	"math/big"

	"github.com/ethereum/go-ethereum/core/types"
	ubiquity "gitlab.com/Blockdaemon/ubiquity/ubiquity-go-client/v1/pkg/client"
)

const (
	networkTestnetC = "testnet-c"
	networkMainnetC = "mainnet-c"
)

var (
	avalancheChains = map[string]*big.Int{
		networkMainnetC: big.NewInt(43114),
		networkTestnetC: big.NewInt(43113),
	}
)

var _ EVMTransaction = (*AvalancheTransaction)(nil)

// Represents an Avalanche Transaction and implements the EVMTransaction interface
type AvalancheTransaction struct {
	Network              string
	PrivateKey           string
	To                   string
	Amount               *big.Float // In Avax
	Nonce                *int64     // Current transaction count
	MaxPriorityFeePerGas *big.Int
	MaxFeePerGas         *big.Int
	Gas                  *big.Int

	// FeeEstimation can be used to indicate whether the Ubiquity
	// /tx/estimate_fee API should be used.
	// When MaxPriorityFeePerGas or MaxFeePerGas is set or when
	// this is set to UbiquityFeeEstimationNone (the default)
	// the fee estimation API is not used.
	FeeEstimation FeeEstimationType
}

func (f AvalancheTransaction) GetNetwork() string                      { return f.Network }
func (f AvalancheTransaction) GetProtocol() string                     { return "avalanche" }
func (f AvalancheTransaction) GetPrivateKey() string                   { return f.PrivateKey }
func (f AvalancheTransaction) GetRecipient() string                    { return f.To }
func (f AvalancheTransaction) GetAmount() *big.Float                   { return f.Amount }
func (f AvalancheTransaction) GetNonce() *int64                        { return f.Nonce }
func (f AvalancheTransaction) GetMaxPriorityFeePerGas() *big.Int       { return f.MaxPriorityFeePerGas }
func (f AvalancheTransaction) GetMaxFeePerGas() *big.Int               { return f.MaxFeePerGas }
func (f AvalancheTransaction) GetGas() *big.Int                        { return f.Gas }
func (f AvalancheTransaction) GetFeeEstimationType() FeeEstimationType { return f.FeeEstimation }

func (f AvalancheTransaction) SetTxProtocolParams(i *ubiquity.TxCreateEvm) *ubiquity.TxCreateProtocol {
	return &ubiquity.TxCreateProtocol{Avalanche: i}
}

// SendAvax creates, signs and sends AVAX transaction. Under the hood it uses Ubiquity TxCreate and TxSend API.
func (s UbiquityTransactionService) SendAvax(ctx context.Context, tx *AvalancheTransaction) (*SendTxResult, error) {
	txRes, err := s.CreateEVMTransaction(ctx, tx)
	if err != nil {
		return nil, err
	}

	privKey, err := GetPrivateKeyECDSA(tx)
	if err != nil {
		return nil, err
	}

	txHash, signedTxData, err := SignAvalancheTransaction(txRes.UnsignedTx, tx.Network, privKey)
	if err != nil {
		return nil, fmt.Errorf("failed to marshal: %v", err)
	}

	txReceipt, err := s.TxSend(ctx, "avalanche", tx.Network, hex.EncodeToString(signedTxData))
	if err != nil {
		return nil, err
	}

	return &SendTxResult{
		TxHash: txHash,
		TxID:   txReceipt.GetId(),
	}, nil
}

// SignAvalancheTransaction signs an unsigned hex encoded transaction for the desired 'network'.
// This function signs a post London hard fork transaction, als referred to as an EIP1559 transaction.
func SignAvalancheTransaction(unsignedHexTx string, network string, privKey *ecdsa.PrivateKey) (txHash string, signedTx []byte, err error) {
	chainID, ok := avalancheChains[network]
	if !ok {
		return "", nil, fmt.Errorf("network %s is not supported", network)
	}

	signer := types.NewLondonSigner(chainID)
	return SignEVMTransactionWithSigner(unsignedHexTx, privKey, signer)
}
