package ubiquitytx

import (
	"context"
	"fmt"
	"math/big"
	"os"
	"testing"

	"github.com/stretchr/testify/mock"
	"github.com/test-go/testify/assert"
	ubiquity "gitlab.com/Blockdaemon/ubiquity/ubiquity-go-client/v1/pkg/client"
	"gitlab.com/Blockdaemon/ubiquity/ubiquity-go-client/v1/pkg/tx/mocks"
)

const (
	// This private key is specifically generated for these unit tests
	evmTestPrivateKey = "8f7b8d880b93a25cd372b1a88763ee619e644aeb73f81e204624f2eff0294812"
	evmTestRecipient  = "0xf31a876d2fd81da268d93212353eeb0ebc325470"
)

const evmFees = `{
		"fast": {
			"max_priority_fee": 1500000000,
			"max_total_fee": 77946555841
		},
		"medium": {
			"max_priority_fee": 1200000000,
			"max_total_fee": 68914371989
		},
		"slow": {
			"max_priority_fee": 1000000000,
			"max_total_fee": 66811237945
		}
}
`

var estimateFeeResponse = ubiquity.InlineResponse200{
	MostRecentBlock: toPointer[int32](12532535),
	EstimatedFees:   &ubiquity.FeeEstimateResponse{},
}

func TestMain(m *testing.M) {

	err := estimateFeeResponse.EstimatedFees.UnmarshalJSON([]byte(evmFees))
	if err != nil {
		panic(err)
	}

	os.Exit(m.Run())

}

func Test_EVM_TxCreation(t *testing.T) {
	t.Run("avalanche/mainnet-c", func(t *testing.T) {
		tx := AvalancheTransaction{
			Network:    "mainnet-c",
			Amount:     big.NewFloat(1.0),
			To:         evmTestRecipient,
			PrivateKey: evmTestPrivateKey,
		}

		testEvmTxCreate(t, tx, "avalanche", "mainnet-c")
	})

	t.Run("ethereum/mainnet", func(t *testing.T) {
		tx := EthereumTransaction{
			Network:       "mainnet",
			Amount:        big.NewFloat(1.0),
			To:            evmTestRecipient,
			PrivateKey:    evmTestPrivateKey,
			FeeEstimation: toPointer(UbiquityFeeEstimationNone),
		}

		testEvmTxCreate(t, tx, "ethereum", "mainnet")
	})

	t.Run("fantom/mainnet", func(t *testing.T) {
		tx := FantomTransaction{
			Network:    "mainnet",
			Amount:     big.NewFloat(1.0),
			To:         evmTestRecipient,
			PrivateKey: evmTestPrivateKey,
		}

		testEvmTxCreate(t, tx, "fantom", "mainnet")
	})

	t.Run("polygon/mainnet", func(t *testing.T) {
		tx := PolygonTransaction{
			Network:    "mainnet",
			Amount:     big.NewFloat(1.0),
			To:         evmTestRecipient,
			PrivateKey: evmTestPrivateKey,
		}

		testEvmTxCreate(t, tx, "polygon", "mainnet")
	})
}

func testEvmTxCreate(t *testing.T, tx EVMTransaction, protocol, network string) {
	txAPIMock := mocks.NewTransactionsAPI(t)
	txCreateRequest := ubiquity.ApiTxCreateRequest{ApiService: txAPIMock}

	txAPIMock.EXPECT().TxCreate(mock.Anything, protocol, network).Return(txCreateRequest)
	txAPIMock.EXPECT().TxCreateExecute(mock.AnythingOfType("ubiquity.ApiTxCreateRequest")).Return(ubiquity.UnsignedTx{UnsignedTx: testUnsignedTxString(tx.GetProtocol(), tx.GetNetwork())}, nil, nil)

	api := NewService(txAPIMock, nil)
	resp, err := api.CreateEVMTransaction(context.TODO(), tx)
	assert.NoError(t, err)
	assert.Equal(t, testUnsignedTxString(protocol, network), resp.UnsignedTx)
}

func testUnsignedTxString(protocol, network string) string {
	return fmt.Sprintf("unit-test-%s-%s-unsigned-tx", protocol, network)
}

func Test_EVM_CreateTx_WithEstimates(t *testing.T) {
	t.Run("avalanche/mainnet-c", func(t *testing.T) {
		tx := AvalancheTransaction{
			Network:       "mainnet-c",
			Amount:        big.NewFloat(1.0),
			To:            evmTestRecipient,
			PrivateKey:    evmTestPrivateKey,
			FeeEstimation: UbiquityFeeEstimationMedium,
		}

		testEvmTxCreateWithFeeEstimates(t, tx, "avalanche", "mainnet-c")
	})

	t.Run("ethereum/mainnet", func(t *testing.T) {
		tx := EthereumTransaction{
			Network:    "mainnet",
			Amount:     big.NewFloat(1.0),
			To:         evmTestRecipient,
			PrivateKey: evmTestPrivateKey,
		}

		testEvmTxCreateWithFeeEstimates(t, tx, "ethereum", "mainnet")
	})

	t.Run("fantom/mainnet", func(t *testing.T) {
		tx := FantomTransaction{
			Network:       "mainnet",
			Amount:        big.NewFloat(1.0),
			To:            evmTestRecipient,
			PrivateKey:    evmTestPrivateKey,
			FeeEstimation: UbiquityFeeEstimationMedium,
		}

		testEvmTxCreateWithFeeEstimates(t, tx, "fantom", "mainnet")
	})

	t.Run("polygon/mainnet", func(t *testing.T) {
		tx := PolygonTransaction{
			Network:       "mainnet",
			Amount:        big.NewFloat(1.0),
			To:            evmTestRecipient,
			PrivateKey:    evmTestPrivateKey,
			FeeEstimation: UbiquityFeeEstimationMedium,
		}

		testEvmTxCreateWithFeeEstimates(t, tx, "polygon", "mainnet")
	})
}

func testEvmTxCreateWithFeeEstimates(t *testing.T, tx EVMTransaction, protocol, network string) {
	txAPIMock := mocks.NewTransactionsAPI(t)
	txCreateRequest := ubiquity.ApiTxCreateRequest{ApiService: txAPIMock}

	txAPIMock.EXPECT().TxCreate(mock.Anything, protocol, network).Return(txCreateRequest)
	txAPIMock.EXPECT().TxCreateExecute(mock.AnythingOfType("ubiquity.ApiTxCreateRequest")).Return(ubiquity.UnsignedTx{UnsignedTx: testUnsignedTxString(tx.GetProtocol(), tx.GetNetwork())}, nil, nil)

	txFeeMock := mocks.NewFeeEstimatorAPI(t)
	feeEstimateRequest := ubiquity.ApiGetFeeEstimateRequest{ApiService: txFeeMock}

	txFeeMock.EXPECT().GetFeeEstimate(mock.Anything, protocol, network).Return(feeEstimateRequest)
	txFeeMock.EXPECT().GetFeeEstimateExecute(mock.AnythingOfType("ubiquity.ApiGetFeeEstimateRequest")).Return(estimateFeeResponse, nil, nil)

	api := NewService(txAPIMock, txFeeMock)
	resp, err := api.CreateEVMTransaction(context.TODO(), tx)
	assert.NoError(t, err)
	assert.Equal(t, testUnsignedTxString(protocol, network), resp.UnsignedTx)
}
